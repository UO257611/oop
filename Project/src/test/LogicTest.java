package test;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import logic.ModuleGrade;
import logic.Name;
import logic.Student;
import logic.Teacher;

class LogicTest {

	@BeforeEach
	void setUp() throws Exception {
	}

	@Test
	void testName()
	{
		Name theName = new Name("Eduardo", "Lamas", "Suárez");
		assertEquals("Eduardo", theName.getFirstName());
		assertEquals("Lamas", theName.getMiddleName());
		assertEquals("Suárez", theName.getLastName());
		
		theName.setFirstName("Jhon");
		assertEquals("Jhon", theName.getFirstName());
		
	}
	
	@Test
	void testStudent()
	{
		Student theStudent = new Student(0, new Name("Eduardo", "Lamas", "Suárez"), "navesllanes@gmail.com", 649168068, "11/11/98", new ArrayList<ModuleGrade>());
		
		assertEquals(0,	theStudent.getStudentIdentifier());
		assertEquals("navesllanes@gmail.com", theStudent.getEmail());
		assertEquals(649168068, theStudent.getPhone());
		assertEquals("11/11/98", theStudent.getStudentDob());
		assertEquals(0, theStudent.getModules().size());
		
		theStudent.addModule(0, "oop", 100);
		assertEquals(1, theStudent.getModules().size());	
	}
	
	@Test
	void testTeacher()
	{
		
		Teacher theTeacher = new Teacher(0, new Name("Eduardo", "Lamas", "Suárez"), "navesllanes@gmail.com" , 649168068, "Software dev");
		assertEquals(0,	theTeacher.getTeacherIdentifier());
		assertEquals("navesllanes@gmail.com", theTeacher.getEmail());
		assertEquals(649168068, theTeacher.getPhone());
		assertEquals("Software dev", theTeacher.getDegreeQualification());
		
	}
	
	@Test 
	void testModuleGrade()
	{
		ModuleGrade module = new ModuleGrade(0, "oop", 100);
		assertEquals(module.getGrade(), 100);
		assertEquals("oop", module.getModuleName());
		assertEquals(0,module.getModuleIdentifier());
		
		module.setGrade(10);
		assertEquals(10, module.getGrade());
		
		
	}

}
