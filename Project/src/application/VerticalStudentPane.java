package application;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import logic.ClassGroup;
import logic.DatabaseInterface;
import logic.ModuleGrade;
import logic.Name;
import logic.PersonalizedPanelWithListView;
import logic.Student;


/**
 * This class is used to  modularize the application
 * 
 * The aim of this class is to set up the  panel that adds a student into the dtabase
 * @author R00171724
 *
 */
public class VerticalStudentPane extends VBox implements PersonalizedPanelWithListView
{
	BorderPane root;
	VBox pnGeneral;
	ClassGroup theClassGroup;
	DatabaseInterface databaseManager;
	ListView<ModuleGrade> lsModuleGrade;
	
	/**
	 * General constructor
	 * 
	 * @param pnGeneral , the panel where the main menu is
	 * @param root, the root window
	 * @param databaseManager, the database manager
	 */
	public VerticalStudentPane(VBox pnGeneral, BorderPane root, DatabaseInterface manager)
	{
		super();
		this.pnGeneral = pnGeneral; 
		this.root = root;
		this.getChildren().add(getTheActualPane());
		this.databaseManager = manager;

		this.setSpacing(40);
	}
	

	/**
	 * This method set up the panel 
	 * @return the verticla panel all already set up
	 */
	public VBox getTheActualPane() {
		VBox pnStudent = new VBox();
		
		Label lblFirstName = new Label("First Name:");
		TextField txtFirstName = new TextField();
		Label lblMiddleName = new Label("Middle Name:");
		TextField txtMiddleName = new TextField();
		Label lblLastName = new Label("Last Name:");
		TextField txtLastName = new TextField();
		Label lblEmail = new Label("Email:");
		TextField txtEmail = new TextField();
		Label lblPhone = new Label("Phone:");
		TextField txtPhone = new TextField();
		Label lblStudentDob = new Label("Student Dob:");
		TextField txtDayDOB = new TextField();
		TextField txtMonthDOB = new TextField();
		TextField txtYearDOB = new TextField();
		
		Label lblModule = new Label("Module Name:");
		TextField txtModuleName = new TextField();
		Label lblModuleGrade = new Label("Module Grade:");
		TextField txtModuleGrade = new TextField();
		Button btnAddModule = new Button("Add Module");
		
		txtDayDOB.setPrefWidth(50);
		txtMonthDOB.setPrefWidth(50);
		txtYearDOB.setPrefWidth(50);
		
		HBox pnHorizontal0 = new HBox();
		pnHorizontal0.getChildren().addAll(lblFirstName, txtFirstName, lblMiddleName, txtMiddleName, lblLastName, txtLastName);
		pnHorizontal0.setAlignment(Pos.CENTER);
		pnHorizontal0.setSpacing(10);
				
		HBox pnHorizontal1 = new HBox();
		pnHorizontal1.getChildren().addAll(lblPhone, txtPhone, lblEmail, txtEmail);
		pnHorizontal1.setAlignment(Pos.CENTER);
		pnHorizontal1.setSpacing(10);
		
		HBox pnHorizontal2 = new HBox();
		pnHorizontal2.getChildren().addAll( lblStudentDob, txtDayDOB, txtMonthDOB, txtYearDOB);
		pnHorizontal2.setAlignment(Pos.CENTER);
		pnHorizontal2.setSpacing(10);
		
		HBox pnHorizontal3 = new HBox();
		pnHorizontal3.getChildren().addAll(lblModule, txtModuleName, lblModuleGrade, txtModuleGrade, btnAddModule);
		pnHorizontal3.setAlignment(Pos.CENTER);
		pnHorizontal3.setSpacing(10);
		
		Label lblModuleList = new Label("Module List");
		
		lsModuleGrade = new ListView<ModuleGrade>();
		ObservableList<ModuleGrade> observableModuleList =  FXCollections.observableArrayList();
		lsModuleGrade.setItems(observableModuleList);
		
		Button btnCreateStudent = new Button("Create Student");
		Button btnExit = new Button("Go Back");
		HBox pnControlButton = new HBox();
		pnControlButton.getChildren().addAll(btnExit, btnCreateStudent);
		pnControlButton.setAlignment(Pos.CENTER);
		pnControlButton.setSpacing(30);
		
		btnAddModule.setOnAction(x -> {
			if (!txtModuleName.getText().isEmpty() && !txtModuleGrade.getText().isEmpty() )
				try {
					int theMark = Integer.parseInt(txtModuleGrade.getText());
					if(theMark >=0 && theMark <=100)
						lsModuleGrade.getItems().add(new ModuleGrade(databaseManager.getNextPrimaryKey(databaseManager.MODULE_KEY_COLUMN) ,txtModuleName.getText(), Integer.parseInt(txtModuleGrade.getText())));
				}
			catch(Exception e)
			{
				System.out.println("Error creating the module");
			}
				});
		
		btnCreateStudent.setOnAction(x -> {
			if (!txtEmail.getText().isEmpty() && !txtFirstName.getText().isEmpty() && 
					!txtMiddleName.getText().isEmpty() && !txtLastName.getText().isEmpty() 
					&& checkDOB(txtDayDOB, txtMonthDOB, txtYearDOB) && !txtPhone.getText().isEmpty() && isAValidEmail(txtEmail)&& isAValidPhone(txtPhone))
			{
				
				
				
				try {
					int studentKey = databaseManager.getNextPrimaryKey(databaseManager.STUDENT_KEY_COLUMN);
					System.out.println("Student key: " + studentKey);
					ArrayList<ModuleGrade> copyListModules = new ArrayList<>();
					lsModuleGrade.getItems().forEach(t -> copyListModules.add(t));
					Student tempStudent = new Student(studentKey, new Name(txtFirstName.getText(), txtMiddleName.getText(), txtLastName.getText()), txtEmail.getText(), 
							Integer.parseInt(txtPhone.getText()), txtDayDOB.getText() +"/" +txtMonthDOB.getText()+"/"+txtYearDOB.getText(), copyListModules);
					theClassGroup.getClassGroup().add(tempStudent);
					databaseManager.addData(tempStudent);
					((ListView)(pnGeneral.getChildren().get(1))).refresh();

					databaseManager.addStudentClassRelation(tempStudent.getStudentIdentifier(), theClassGroup.getClassIdentifier());
					
					clearPane(new TextField[]{txtEmail,txtFirstName, txtLastName, txtMiddleName, txtModuleName , txtModuleGrade, txtPhone, txtDayDOB,txtMonthDOB, txtYearDOB}, lsModuleGrade);
					
			}catch(Exception e)
			{
					System.out.println("Error creating the student");
					e.printStackTrace();
			}
			}
		});
		
		btnExit.setOnAction(x -> root.setCenter(pnGeneral));
		
		pnStudent.getChildren().addAll(pnHorizontal0,pnHorizontal1,pnHorizontal2, pnHorizontal3, lblModuleList, lsModuleGrade, pnControlButton);
		pnStudent.setAlignment(Pos.CENTER);
		pnStudent.setSpacing(10);
		
		return pnStudent;
		
	}
	
	/**
	 * This method clears all the textfields of the panel
	 * @param txtField, array with all the textfields of the panel
	 * @param lsModules, listview with all the modules of the last new student
	 */
	private void clearPane(TextField[] txtField, ListView lsModules) {
		Arrays.asList(txtField).forEach(x -> x.clear());;
		lsModules.getItems().clear();
	}
	
	/**
	 * check date of birth
	 * @param day, textfield with the day
	 * @param month, textfield with the month
	 * @param year, textfield with the yewar
	 * @return true if is the all ok, false if not
	 */
	private boolean checkDOB(TextField day, TextField month, TextField year)
	{
		try {
			
			if(!day.getText().isEmpty() && !month.getText().isEmpty() && !year.getText().isEmpty())
			{
				
				int theDay = Integer.parseInt(day.getText());
				int theMonth = Integer.parseInt(month.getText());
				int theYear = Integer.parseInt(year.getText());
				
				if(theDay <= 31 & theMonth <= 12 && theDay>=1 && theMonth >=1&& theYear>=1900 && theYear <=2019)
					return true;
				
			}
			return false;
			
			
		}catch(Exception e)
		{
			return false;
		}
		
	}
	

	/**
	 * Check if  the phone is valid
	 * @param txtPhone, textField with the phone
	 * @return true if is valid, false if not
	 */
	private boolean isAValidPhone(TextField txtPhone) {
		try {
			
			Integer.parseInt(txtPhone.getText());
			return true;
		}catch(Exception e)
		{
			return false;
		}
	}

	/**
	 * Checks if the email is valid
	 * @param email, textfield with the email
	 * @return true if is valid, false if not
	 */
	private boolean isAValidEmail(TextField email) 
	{
		 String[] splittedEmail1 = email.getText().split("@");
		 System.out.println(splittedEmail1.length);
		 if(splittedEmail1.length == 2)
		 {
			 String[] splittedEmail2 = splittedEmail1[1].split("\\.");
			 System.out.println(splittedEmail2.length);
			 if(splittedEmail2.length == 2)
			 {
				 return true;
			 }
		 }
	     return false;
	}

	@Override
	public <T> void setUpSpecificObject(T currentClassGroup) {
		
			this.theClassGroup = (ClassGroup) currentClassGroup;
		
	}

	@Override
	public void refreshListView() {
		lsModuleGrade.refresh();
		
	}


}
