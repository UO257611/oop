package application;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import logic.ClassGroup;
import logic.DatabaseInterface;
import logic.PersonalizedPanelWithListView;
import logic.Student;
/**
 * This class is used to modularize the application
 * 
 *  The aim of this class is set up the panel where all the students are listed  
 * @author R00171724
 *
 */
public class ListAllStudentsPanel extends VBox implements PersonalizedPanelWithListView
{

	private ArrayList<ClassGroup> allClassGroups = new ArrayList<>();
	private VBox pnGeneral;
	private DatabaseInterface databaseManager;
	private ModifyGradePanel pnModifyGrade;
	private ListView<Student> lsStudents;
	private Label lblTitle ;
	private BorderPane root;

	/***
	 * The constructor of the class
	 * @param pnGeneral , the panel where the main menu is
	 * @param root, the root window
	 * @param databaseManager, the database manager
	 */
	public ListAllStudentsPanel(VBox pnGeneral,BorderPane root, DatabaseInterface databaseManager) {
		this.pnGeneral = pnGeneral;
		this.root = root;
		this.databaseManager = databaseManager;
		this.getChildren().add(setUpPanel());
		pnModifyGrade = new ModifyGradePanel(this, root, databaseManager);

		this.setSpacing(40);
	}

	/**
	 * This method set up the panel 
	 * @return the verticla panel all already set up
	 */
	private VBox setUpPanel() {

		VBox pnGeneralStudents = new VBox();
		pnGeneralStudents.setSpacing(20);
		pnGeneralStudents.setAlignment(Pos.CENTER);
		Button btnGoBack = new Button("Go Back");
		lblTitle = new Label("List with all the students");
		lblTitle.setFont(new Font(20));
		lblTitle.setUnderline(true);
		
		Button btnRemoveStudent = new Button("Remove Student");
		Button btnChangeMarks = new Button("Modify Grade");
		btnChangeMarks.setDisable(true);
		btnRemoveStudent.setDisable(true);
		
		lsStudents = new ListView<Student>();
		ObservableList<Student> observableStudentList = FXCollections.observableArrayList();
		lsStudents.setItems(observableStudentList);
		
		HBox pnButtons = new HBox();
		pnButtons.setSpacing(20);
		pnButtons.setAlignment(Pos.CENTER);
		
		pnButtons.getChildren().addAll(btnGoBack,btnRemoveStudent, btnChangeMarks);
		
		pnGeneralStudents.getChildren().addAll(lblTitle, lsStudents, pnButtons);
		
		
		
		//Button listener that goes back
		btnGoBack.setOnAction(x -> root.setCenter(pnGeneral));

		//Button listener that changes the marks
		btnChangeMarks.setOnAction(x -> changeMarks());
		// Button listener that remoces the student
		btnRemoveStudent.setOnAction(x -> removeStudent());
		
		
		lsStudents.setOnMouseClicked(new EventHandler<MouseEvent>() {


			@Override
			public void handle(MouseEvent event) {
				if (lsStudents.getSelectionModel().getSelectedIndex() >=0)
				{
					btnChangeMarks.setDisable(false);
					btnRemoveStudent.setDisable(false);
				}
				}
			});
		
		
		
		return pnGeneralStudents;

	}

	/**
	 * method used to remove a student
	 */
	private void removeStudent() {
		Student theStudent = lsStudents.getSelectionModel().getSelectedItem();
		databaseManager.deleteStudent(lsStudents.getSelectionModel().getSelectedItem(), true);
		lsStudents.getItems().remove(lsStudents.getSelectionModel().getSelectedIndex());
		lsStudents.refresh();
		allClassGroups.forEach( classGroup -> {
			if (classGroup.getClassGroup().contains(theStudent))
			{
				classGroup.getClassGroup().remove(theStudent);
			}
			
			
		} );
		((ListView)(pnGeneral.getChildren().get(1))).refresh();
	
	}

	/**
	 * Method used to chenge the marks
	 */
	private void changeMarks() {
		pnModifyGrade.setCurrentStudent(lsStudents.getSelectionModel().getSelectedItem());
		root.setCenter(pnModifyGrade);
	}
	
	
	
	
	@Override
	public <T> void setUpSpecificObject(T specificObject) {

		allClassGroups.clear();
		lsStudents.getItems().clear();
		ObservableList<ClassGroup> theClasses = ((ObservableList<ClassGroup>) specificObject);
		theClasses.forEach(x -> allClassGroups.add(x));
		ArrayList<Student> allTheStudents = new ArrayList<>();
		allClassGroups.forEach(c -> c.getClassGroup().forEach(s -> allTheStudents.add(s)));
		Collections.sort(allTheStudents, Comparator.comparing(Student::getFullName));
		allTheStudents.forEach( x -> lsStudents.getItems().add(x));
		lsStudents.refresh();
		
		
	}

	@Override
	public void refreshListView() {
		lsStudents.refresh();
		
	}

}

