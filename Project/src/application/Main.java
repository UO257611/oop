package application;
	
import java.util.ArrayList;
import java.util.List;

import javax.naming.ldap.ManageReferralControl;

import org.apache.derby.impl.sql.execute.CreateConstraintConstantAction;

import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.stage.Stage;
import logic.ClassGroup;
import logic.DatabaseInterface;
import logic.DatabaseManager;
import logic.Name;
import logic.Student;
import logic.Teacher;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

/**
 * This is the main class of the program
 * @author R00171724
 *
 */
public class Main extends Application {
	
	private VBox pnGeneral = new VBox();
	private VerticalStudentPane pnStudentPane;
	private ListAllStudentsPanel pnAllStudents;
	private AddTeacherPanel pnAddTeacher;
	private ListStudentInClassPanel pnStudentInClass;
	private ListAllTeachersPanel pnAllTeacher;
	private DatabaseInterface databaseManager;
	private ListView<ClassGroup> lsClassGroups;
	/**
	 * Method callaed at the begining of the execution of the program
	 */
	@Override
	public void start(Stage primaryStage) {
		try {
			databaseManager = DatabaseManager.getManager();
			BorderPane root = new BorderPane();
			Scene scene = new Scene(root,400,400);
			scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
			primaryStage.setScene(scene);
			primaryStage.show();
			pnAddTeacher = new AddTeacherPanel(pnGeneral, root, databaseManager); 
			pnStudentPane = new VerticalStudentPane(pnGeneral, root, databaseManager);
			pnStudentInClass = new ListStudentInClassPanel(pnGeneral,  root , databaseManager);
			pnAllTeacher = new ListAllTeachersPanel(pnGeneral, root, databaseManager);
			pnAllStudents = new ListAllStudentsPanel(pnGeneral, root, databaseManager);
			
			setupRootPane(root);
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	/**
	 * This method set up the root window of the program
	 * @param root, root borderPane
	 */
	private void setupRootPane(BorderPane root) 
	{
		HBox pnClassCreation = new HBox();
		
		
		Label lblClassNameGroup = new Label("Class group name:");
		TextField txtClassName = new TextField();
		Button btnCreateClassGroup = new Button("Add Class");
		
		pnClassCreation.getChildren().addAll(lblClassNameGroup, txtClassName, btnCreateClassGroup);
		pnClassCreation.setAlignment(Pos.CENTER);
		pnClassCreation.setSpacing(25);
		pnGeneral.setAlignment(Pos.CENTER);
		pnGeneral.setSpacing(25);
		
	
		lsClassGroups = new ListView<>();
		ObservableList<ClassGroup> observableClassGroupList = FXCollections.observableArrayList();
		lsClassGroups.setItems(observableClassGroupList);
		checkIfThereAreExistingGroups(lsClassGroups);
		Button btnAddStudent = new Button("Add Student");
		Button btnRemoveClassGroup = new Button("Remove ClassGroup");
		Button btnAddTeacher = new Button("Add Teacher");
		Button btnListClassGroup = new Button("List Students In Class");
		Button btnListAllStudents = new Button("List All Students");
		Button btnListAllTeachers = new Button("List All Teachers");
		HBox pnAddStudentTeacher = new HBox();
		HBox pnLists = new HBox();
	
		
		pnAddStudentTeacher.getChildren().addAll(btnRemoveClassGroup, btnAddStudent, btnAddTeacher);
		pnLists.getChildren().addAll(btnListAllTeachers,btnListClassGroup, btnListAllStudents);
		pnAddStudentTeacher.setAlignment(Pos.CENTER);
		pnAddStudentTeacher.setSpacing(25);
		pnLists.setAlignment(Pos.CENTER);
		pnLists.setSpacing(25);
		
		
		btnAddStudent.setDisable(true);
		btnListClassGroup.setDisable(true);
		btnRemoveClassGroup.setDisable(true);
		
		btnAddStudent.setOnAction(new EventHandler<ActionEvent>() {
			
			@Override
			public void handle(ActionEvent event) {
				pnStudentPane.setUpSpecificObject(lsClassGroups.getSelectionModel().getSelectedItem());
				root.setCenter(pnStudentPane);
			}
		});
		
		btnAddTeacher.setOnAction(x -> root.setCenter(pnAddTeacher));
		btnCreateClassGroup.setOnAction(x -> creatClassGroup(txtClassName, lsClassGroups));
		btnRemoveClassGroup.setOnAction(x -> removeClassGroup(lsClassGroups));
		
		
		
		lsClassGroups.setOnMouseClicked(new EventHandler<MouseEvent>() {


			@Override
			public void handle(MouseEvent event) {
				if (lsClassGroups.getSelectionModel().getSelectedIndex() >=0)
				{
					btnAddStudent.setDisable(false);
					btnListClassGroup.setDisable(false);
					btnRemoveClassGroup.setDisable(false);
				}
				}
			});
		
		btnListClassGroup.setOnAction( x -> {
			pnStudentInClass.setUpSpecificObject(lsClassGroups.getSelectionModel().getSelectedItem());
			root.setCenter(pnStudentInClass);
		});
		
		btnListAllStudents.setOnAction(x -> {
			pnAllStudents.setUpSpecificObject(lsClassGroups.getItems());
			root.setCenter(pnAllStudents);
		});
		btnListAllTeachers.setOnAction(x -> {
			pnAllTeacher.setUpSpecificObject(null);
			root.setCenter(pnAllTeacher);
		});
		pnGeneral.getChildren().addAll(pnClassCreation, lsClassGroups, pnAddStudentTeacher, pnLists);
		root.setCenter(pnGeneral);
		
	}
	/**
	 * Method that checks if there are existing class groups
	 * @param lsGroups, listview of the class groups
	 */
	private void checkIfThereAreExistingGroups(ListView<ClassGroup> lsGroups)
	{
		List<ClassGroup> existingGroups =  databaseManager.getExistingGroups();
		if( existingGroups.size() >0)
		{
			existingGroups.forEach(x -> lsGroups.getItems().add(x));
		}
				
	}
	
	/**
	 * Method called from the specific button that deletes a class group
	 * 
	 * @param lsClassGroupm listview with the class groups
	 */
	private void removeClassGroup(ListView<ClassGroup> lsClassGroup) 
	{
		databaseManager.deleteClassGroup(lsClassGroup.getSelectionModel().getSelectedItem());
		lsClassGroup.getItems().remove(lsClassGroup.getSelectionModel().getSelectedIndex());
		lsClassGroup.refresh();
	}
	
	/**
	 * This is the method called from a specific button. 
	 * This medthod creates a class group in the database
	 * @param txtClassName, textfield with the class name
	 * @param lsClassGroups, listview with all the existing classes
	 */
	private void creatClassGroup(TextField txtClassName, ListView<ClassGroup> lsClassGroups)
	{
		if(!txtClassName.getText().isEmpty())
		{
			int keyClass = databaseManager.getNextPrimaryKey(databaseManager.CLASS_GROUP_KEY_COLUMN);
			System.out.println("Class key:" + keyClass);
			ClassGroup tempClass = new ClassGroup( keyClass,txtClassName.getText());
			databaseManager.addData(tempClass);
			lsClassGroups.getItems().add(tempClass);				
			lsClassGroups.refresh();
		}
	}
	
	/**
	 * The main of the program
	 * @param args
	 */
	public static void main(String[] args) {
		launch(args);
	}
	
	public void addALotOfData()
	{
		
		for(int i = 0; i< 100000; i++)
			creatClassGroup(new TextField(""), lsClassGroups);
		
		
		
	}
	
}
