package application;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import logic.ClassGroup;
import logic.DatabaseInterface;
import logic.PersonalizedPanelWithListView;
import logic.Student;
import logic.Teacher;


/**
 * This class is used to  modularize the application
 * 
 * The aim of this class is to set up the  panel that list the teachers to the database
 * @author R00171724
 *
 */
public class ListAllTeachersPanel extends VBox implements PersonalizedPanelWithListView
{
	private VBox pnGeneral;
	private ClassGroup currentClassGroup;
	private DatabaseInterface databaseManager;
	private ListView<Teacher> lsTeacher;

	private BorderPane root;

	/***
	 * The constructor of the class
	 * @param pnGeneral , the panel where the main menu is
	 * @param root, the root window
	 * @param databaseManager, the database manager
	 */
	public ListAllTeachersPanel(VBox pnGeneral,BorderPane root, DatabaseInterface databaseManager) {
		this.pnGeneral = pnGeneral;
		this.root = root;
		this.databaseManager = databaseManager;
		this.getChildren().add(setUpPanel());
	
		this.setSpacing(40);

	}
	/**
	 * This method set up the panel 
	 * @return the verticla panel all already set up
	 */
	private VBox setUpPanel() {
		VBox pnTeacherListGeneral = new VBox();
		
		pnTeacherListGeneral.setSpacing(20);
		pnTeacherListGeneral.setAlignment(Pos.CENTER);
		Button btnGoBack = new Button("Go Back");
		Label lblTitle = new Label("Full Teacher List: ");
		lblTitle.setFont(new Font(35));
		lblTitle.setUnderline(true);
		
		
		Button btnChangeMarks = new Button("Modify Grade");
		btnChangeMarks.setDisable(true);
		
		lsTeacher = new ListView<Teacher>();
		ObservableList<Teacher> observableTeacherList = FXCollections.observableArrayList();
		lsTeacher.setItems(observableTeacherList);
		
		HBox pnButtons = new HBox();
		pnButtons.setSpacing(20);
		pnButtons.setAlignment(Pos.CENTER);
		
		pnButtons.getChildren().addAll(btnGoBack, btnChangeMarks);
		
		pnTeacherListGeneral.getChildren().addAll(lblTitle, lsTeacher, pnButtons);
		
		
		
		
		btnGoBack.setOnAction(x -> root.setCenter(pnGeneral));
		
	
		
		
		return pnTeacherListGeneral;
	}
	
	


	@Override
	public <T> void setUpSpecificObject(T specificObject) {
		
		List<Teacher> allTeachers = databaseManager.getTeacherList();
		lsTeacher.getItems().clear();
		Collections.sort(allTeachers, Comparator.comparing(Teacher::getFullName));
		lsTeacher.getItems().addAll(allTeachers);
		lsTeacher.refresh();
		
		
	}

	@Override
	public void refreshListView() {
		lsTeacher.refresh();
	}


}
