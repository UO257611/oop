package application;


import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import logic.DatabaseInterface;
import logic.ModuleGrade;
import logic.PersonalizedPanelWithListView;
import logic.Student;



/**
 * This class is used to  modularize the application
 * 
 * The aim of this class is to set up the  panel that changes the grades in the database
 * @author R00171724
 *
 */
public class ModifyGradePanel extends HBox implements PersonalizedPanelWithListView
{
	private VBox pnLastPanel;
	private BorderPane root;
	private DatabaseInterface databaseManager;
	private Student currentStudent;
	private Label lblStudentInfo;
	private ListView<ModuleGrade> lsModules;
	

	/***
	 * The constructor of the class
	 * @param pnLastPanel , the panel that is was in the mainwindow before the modify grades panel
	 * @param root, the root window
	 * @param databaseManager, the database manager
	 */
	public ModifyGradePanel(VBox pnLastPanel,BorderPane root, DatabaseInterface databaseManager)
	{
		
		
		this.pnLastPanel = pnLastPanel;
		this.root = root;
		this.databaseManager = databaseManager;
		this.getChildren().add(setUpPanel());
		this.setAlignment(Pos.CENTER);

		this.setSpacing(40);
		
	}
	
	/**
	 * This method set up the panel 
	 * @return the vertical panel all already set up
	 */
	private VBox setUpPanel()
	{
		VBox pnGeneral = new VBox();
		pnGeneral.setAlignment(Pos.CENTER);
		pnGeneral.setSpacing(20);
		
		HBox pnMarks = new HBox();
		pnMarks.setAlignment(Pos.CENTER);
		pnMarks.setSpacing(20);
		
		HBox pnGoBack = new HBox();
		pnGoBack.setAlignment(Pos.CENTER_LEFT);
		pnGoBack.setSpacing(20);
		
		
		lblStudentInfo  = new Label();
		lblStudentInfo.setFont(new Font(30));
		lblStudentInfo.setUnderline(true);
		
		lsModules = new ListView();
		ObservableList<ModuleGrade> observableModuleList = FXCollections.observableArrayList();
		lsModules.setItems(observableModuleList);
		Label lblNewMark = new Label("New grade: ");
		TextField txtNewMark = new TextField();
		Button btnChangeGrade = new Button("Change Grade");
		btnChangeGrade.setDisable(true);
		
		Button btnGoBack = new Button("Go Back");
		
		pnGoBack.getChildren().add(btnGoBack);
		
		pnMarks.getChildren().addAll(lblNewMark, txtNewMark, btnChangeGrade);
		
		pnGeneral.getChildren().addAll(lblStudentInfo, lsModules, pnMarks, pnGoBack); 

		btnGoBack.setOnAction(x -> root.setCenter(pnLastPanel));
		
		lsModules.setOnMouseClicked(new EventHandler<MouseEvent>() {


			@Override
			public void handle(MouseEvent event) {
				if (lsModules.getSelectionModel().getSelectedIndex() >=0)
				{
					btnChangeGrade.setDisable(false);
					txtNewMark.setText(String.valueOf(lsModules.getSelectionModel().getSelectedItem().getGrade()));
				}
				}
			});
		
		btnChangeGrade.setOnAction(x -> changeMark(lsModules.getSelectionModel().getSelectedItem(), txtNewMark.getText()));
		return pnGeneral;
		
	}
	
	/**
	 * This method changes the marks of a module in the database
	 * @param theModule, the module object of the student
	 * @param newGrade, the new grade
	 */
	private void changeMark( ModuleGrade theModule, String newGrade)
	{
			try {
					
				int  newGradeInt = Integer.parseInt(newGrade);
				theModule.setGrade(newGradeInt);
				databaseManager.modifyGrade(theModule, newGradeInt);
				lsModules.refresh();
				((PersonalizedPanelWithListView)pnLastPanel).refreshListView();
				
			}catch(Exception e)
			{
				System.out.println("You wrote an invalid grade");
			}
	}
	
	
	/**
	 * changes the selected student that is going to change the grade
	 * @param student, the new student
	 */
	public void setCurrentStudent(Student student)
	{
		currentStudent = student;
		
		lblStudentInfo.setText("Information about: " + student.getName().getFirstName() +" "+ student.getName().getMiddleName() + " " + student.getName().getLastName());

		lsModules.getItems().clear();
		
		student.getModules().forEach(x -> lsModules.getItems().add(x));
		lsModules.refresh();	
		
	}

	@Override
	public <T> void setUpSpecificObject(T specificObject) {
		
		Student student = (Student) specificObject;
		currentStudent = student;
		
		lblStudentInfo.setText("Information about: " + student.getName().getFirstName() +" "+ student.getName().getMiddleName() + " " + student.getName().getLastName());

		lsModules.getItems().clear();
		
		student.getModules().forEach(x -> lsModules.getItems().add(x));
		lsModules.refresh();	
		
	}

	@Override
	public void refreshListView() {
		lsModules.refresh();
		
	}
	
	
}
