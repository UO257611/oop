package application;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import logic.ClassGroup;
import logic.DatabaseInterface;
import logic.ModuleGrade;
import logic.PersonalizedPanelWithListView;
import logic.Student;
/**
 * This class is used to modularize the application
 * 
 *  The aim of this class is set up the panel where all the students in a class are listed  
 * @author R00171724
 *
 */
public class ListStudentInClassPanel extends VBox implements PersonalizedPanelWithListView{
	private VBox pnGeneral;
	private ClassGroup currentClassGroup;
	private DatabaseInterface databaseManager;
	private ListView<Student> lsStudents;
	private Label lblTitle ;
	private ModifyGradePanel pnModifyGrade;
	private BorderPane root;


	/***
	 * The constructor of the class
	 * @param pnGeneral , the panel where the main menu is
	 * @param root, the root window
	 * @param databaseManager, the database manager
	 */
	public ListStudentInClassPanel(VBox pnGeneral,BorderPane root, DatabaseInterface databaseManager){
		this.pnGeneral = pnGeneral;
		this.root = root;
		this.databaseManager = databaseManager;
		this.getChildren().add(setUpPanel());
		this.pnModifyGrade = new ModifyGradePanel(this, root, databaseManager);

		this.setSpacing(40);

	}
	
	/**
	 * This method set up the panel 
	 * @return the verticla panel all already set up
	 */
	private VBox setUpPanel() {

		VBox pnGeneralStudents = new VBox();
		pnGeneralStudents.setSpacing(20);
		pnGeneralStudents.setAlignment(Pos.CENTER);
		Button btnGoBack = new Button("Go Back");
		lblTitle = new Label();
		lblTitle.setFont(new Font(35));
		lblTitle.setUnderline(true);
		
		Button btnRemove = new Button("Remove Student");
		btnRemove.setDisable(true);
		
		Button btnChangeMarks = new Button("Modify Grade");
		btnChangeMarks.setDisable(true);
		
		lsStudents = new ListView<Student>();
		ObservableList<Student> observableStudentList = FXCollections.observableArrayList();
		lsStudents.setItems(observableStudentList);
		
		HBox pnButtons = new HBox();
		pnButtons.setSpacing(20);
		pnButtons.setAlignment(Pos.CENTER);
		
		pnButtons.getChildren().addAll(btnGoBack,btnRemove, btnChangeMarks);
		
		pnGeneralStudents.getChildren().addAll(lblTitle, lsStudents, pnButtons);
		
		
		btnRemove.setOnAction(x -> removeStudent());
		
		btnGoBack.setOnAction(x -> root.setCenter(pnGeneral));
		
		lsStudents.setOnMouseClicked(new EventHandler<MouseEvent>() {


			@Override
			public void handle(MouseEvent event) {
				if (lsStudents.getSelectionModel().getSelectedIndex() >=0)
				{
					btnChangeMarks.setDisable(false);
					btnRemove.setDisable(false);
				}
				}
			});
		
		btnChangeMarks.setOnAction(x -> changeMarks());
		
		return pnGeneralStudents;

	}
	
	@Override
	public <T>void setUpSpecificObject( T currentClassGroup)
	{
		this.currentClassGroup =  (ClassGroup) currentClassGroup;
		lsStudents.getItems().clear();
		lblTitle.setText("List of students for class " + this.currentClassGroup.getClassName() + ":");
		
		List<Student> sortedStudents = this.currentClassGroup.getClassGroup();
		Collections.sort(sortedStudents, Comparator.comparing(Student::getFullName));
		
		sortedStudents.forEach( x -> lsStudents.getItems().add(x));
		
	}


	@Override
	public void refreshListView() {
		lsStudents.refresh();
		
	}

	/**
	 * This is the method called in the specific button to change the marks of a student
	 */
	private void changeMarks() {
		pnModifyGrade.setCurrentStudent(lsStudents.getSelectionModel().getSelectedItem());
		root.setCenter(pnModifyGrade);
	}
	
	

	/**
	 * This is the method that is called from the specific button in order
	 * to remove a student from the database
	 */
	private void removeStudent() {
		databaseManager.deleteStudent(lsStudents.getSelectionModel().getSelectedItem(), true);
		currentClassGroup.getClassGroup().remove(lsStudents.getSelectionModel().getSelectedItem());
		lsStudents.getItems().remove(lsStudents.getSelectionModel().getSelectedIndex());
		lsStudents.refresh();
		((ListView)(pnGeneral.getChildren().get(1))).refresh();
	
	}


}
