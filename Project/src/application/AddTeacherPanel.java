package application;

import java.util.Arrays;

import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import logic.DatabaseInterface;
import logic.Name;
import logic.Student;
import logic.Teacher;
/**
 * This class is used to  modularize the application
 * The aim of this class is to set up the  panel that adds the teachers to the database
 * @author R00171724
 *
 */
public class AddTeacherPanel extends VBox
{

	private VBox pnGeneral;
	private DatabaseInterface databaseManager;
	private BorderPane root;
	/**
	 * This is the constructor of the class
	 * @param pnGeneral the main pane where is the class groups
	 * @param root, the root window
	 * @param databaseManager, the database manager
	 */
	public AddTeacherPanel(VBox pnGeneral,BorderPane root, DatabaseInterface databaseManager)
	{
		this.pnGeneral = pnGeneral;
		this.root = root;
		this.databaseManager = databaseManager;
		
		this.getChildren().add(setUpPanel());
		this.setAlignment(Pos.CENTER);
		this.setSpacing(40);
		
		
		
	}
	
	/**
	 * This method set up the panel so can be used correctly
	 * @return The panel that has been set up
	 */
	public VBox setUpPanel()
	{
		VBox pnTeacherGeneral = new VBox();
		pnTeacherGeneral.setSpacing(40);

		Label lblFirstName = new Label("First Name:");
		TextField txtFirstName = new TextField();
		Label lblMiddleName = new Label("Middle Name:");
		TextField txtMiddleName = new TextField();
		Label lblLastName = new Label("Last Name:");
		TextField txtLastName = new TextField();
		Label lblEmail = new Label("Email:");
		TextField txtEmail = new TextField();
		Label lblPhone = new Label("Phone:");
		TextField txtPhone = new TextField();
		Label lblDegreeQualification = new Label("Degree Qualification: ");
		TextField txtDegreeQualification = new TextField();
		

		
		HBox pnHorizontal0 = new HBox();
		pnHorizontal0.getChildren().addAll(lblFirstName, txtFirstName, lblMiddleName, txtMiddleName, lblLastName, txtLastName);
		pnHorizontal0.setAlignment(Pos.CENTER);
		pnHorizontal0.setSpacing(10);
				
		HBox pnHorizontal1 = new HBox();
		pnHorizontal1.getChildren().addAll(lblPhone, txtPhone, lblEmail, txtEmail, lblDegreeQualification, txtDegreeQualification);
		pnHorizontal1.setAlignment(Pos.CENTER);
		pnHorizontal1.setSpacing(10);
		
		Button btnCreateTeacher = new Button("Create Teacher");
		Button btnExit = new Button("Go Back");
		
		HBox pnControlButton = new HBox();
		pnControlButton.getChildren().addAll(btnExit, btnCreateTeacher);
		pnControlButton.setAlignment(Pos.CENTER);
		pnControlButton.setSpacing(30);
		
		//The exit button
		btnExit.setOnAction(x -> root.setCenter(pnGeneral));		
		//This button create the teacher
		btnCreateTeacher.setOnAction(x -> {
			if (!txtEmail.getText().isEmpty() && !txtFirstName.getText().isEmpty() && 
					!txtMiddleName.getText().isEmpty() && !txtLastName.getText().isEmpty() 
					&& !txtPhone.getText().isEmpty() && isAValidEmail(txtEmail)&& isAValidPhone(txtPhone))
			{
				
				
				
				try {
					int teacherKey = databaseManager.getNextPrimaryKey(databaseManager.TEACHER_KEY_COLUMN);
					System.out.println("Student key: " + teacherKey);
					Teacher tempTeacher = new Teacher(teacherKey, new Name(txtFirstName.getText(), txtMiddleName.getText(), txtLastName.getText()), txtEmail.getText(), 
							Integer.parseInt(txtPhone.getText()), txtDegreeQualification.getText());
					databaseManager.addData(tempTeacher);

					clearPane(new TextField[]{txtEmail,txtFirstName, txtLastName, txtMiddleName, txtDegreeQualification , txtPhone} );
					
			}catch(Exception e)
			{
					System.out.println("Error creating the student");
			}
			}
		});

		pnTeacherGeneral.getChildren().addAll(pnHorizontal0, pnHorizontal1, pnControlButton);
				
		
		return pnTeacherGeneral;
	}

	
	/**
	 * THis is a method that checks the validity of the phone number
	 * @param txtPhone, textfield where the phone is written
	 * @return true if ,it is valid, false if not
	 */
	private boolean isAValidPhone(TextField txtPhone) {
		try {
			
			Integer.parseInt(txtPhone.getText());
			return true;
		}catch(Exception e)
		{
			return false;
		}
	}

	/**
	 * The method check the validity of an email
	 * @param email textfield where the email has been written
	 * @return true if it is valid, false if not
	 */
	private boolean isAValidEmail(TextField email) 
	{
		 String[] splittedEmail1 = email.getText().split("@");
		 System.out.println(splittedEmail1.length);
		 if(splittedEmail1.length == 2)
		 {
			 String[] splittedEmail2 = splittedEmail1[1].split("\\.");
			 System.out.println(splittedEmail2.length);
			 if(splittedEmail2.length == 2)
			 {
				 return true;
			 }
		 }
		 System.out.println("------");
	     return false;
	}
	

	/**
	 * Method that clears all the textfields
	 * @param txtField
	 */
	private void clearPane(TextField[] txtField) {
		Arrays.asList(txtField).forEach(x -> x.clear());
	}
}
