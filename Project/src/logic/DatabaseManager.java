package logic;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ScheduledExecutorService;
import java.util.stream.Collectors;


/**
 * 
 * Database manager that was created using a Singleton  and Mediator patterns;
 * 
 * This class is also an intermediate between the database and the program
 * 
 * @author Eduardo Lamas Suárez, R00171724
 *
 */
public class DatabaseManager implements DatabaseInterface
{
	private Connection conn;
	private static final String DRIVER = "org.apache.derby.jdbc.EmbeddedDriver";
    private static final String DBURL = "jdbc:derby:UniversityDB;create=true;";
    private static DatabaseManager theManager;
    
    /**
     * Private constructor, used in the Singleton patter
     */
	private DatabaseManager()
	{
		getConnection();
		createTables();
	}
	
	/**
	 * Public constructor or getter depensds if its already created the dabase manager object
	 * @return database manager
	 */
	public static DatabaseManager getManager()
	{
		
		if(theManager == null)
		{
			theManager = new DatabaseManager();
		}
		return theManager;
		
		
	}
	
	
    /**
     * Returns the connection of the database
     */
	private  void getConnection() {
		try {
			Class.forName(DRIVER).newInstance();
			conn = DriverManager.getConnection(DBURL);
			
			if(conn != null)
			{
				System.out.println("Connection successful");
				
			}
			
			
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		
	}
	
	/**
	 * Remove all the tables from the database, only used for testing
	 */
	private void dropAllTables()
	{
		try {
		Statement stmt = conn.createStatement();
		
		stmt.executeUpdate("DROP TABLE GOES" );
		stmt.executeUpdate("DROP TABLE STUDIES" );
		stmt.executeUpdate("DROP TABLE CLASS_GROUP" );
		stmt.executeUpdate("DROP TABLE TEACHER" );
		stmt.executeUpdate("DROP TABLE MODULE_GRADE" );
		stmt.executeUpdate("DROP TABLE PRIMARY_KEY_COUNTERS" );
		stmt.executeUpdate("DROP TABLE STUDENT" );
		
		}catch(SQLException e)
		{
			System.out.println(e.getMessage());
		}
		
		
	}
	/**
	 * This method creates all the needed tables of the database
	 */
	private  void createTables() 
	{
		try {
			Statement stmt = conn.createStatement();
			//dropAllTables();
			createNewTable(stmt, "CREATE TABLE STUDENT (STUDENT_ID INT PRIMARY KEY, FIRST_NAME VARCHAR(50),"
					+ "MIDDLE_NAME VARCHAR(50), LAST_NAME VARCHAR(50), PHONE INT, EMAILBODY VARCHAR(50), EMAILCOMPANY VARCHAR(50), EMAILTERMINATION VARCHAR(10), STUDENT_DOB VARCHAR(12))", "STUDENT");
			createNewTable(stmt, "CREATE TABLE TEACHER (TEACHER_ID INT PRIMARY KEY, FIRST_NAME VARCHAR(50), " + 
					"MIDDLE_NAME VARCHAR(50), LAST_NAME VARCHAR(50), PHONE INT,EMAILBODY VARCHAR(50), EMAILCOMPANY VARCHAR(50), EMAILTERMINATION VARCHAR(10), "
					+ "DEGREE_QUALIFICATION VARCHAR(50))", "TEACHER");
			createNewTable(stmt, "CREATE TABLE MODULE_GRADE (MODULE_ID INT PRIMARY KEY, MODULE_NAME"
					+ " VARCHAR(12), GRADE INT )", "MODULE_GRADE");
			createNewTable(stmt, "CREATE TABLE CLASS_GROUP (CLASS_GROUP_ID INT PRIMARY KEY, "
					+ "CLASS_NAME VARCHAR(50) )", "CLASS_GROUP");
			createNewTable(stmt, "CREATE TABLE STUDIES (STUDENT_ID INT REFERENCES STUDENT(STUDENT_ID), "
					+ "MODULE_ID INT REFERENCES MODULE_GRADE(MODULE_ID) )", "STUDIES");
			createNewTable(stmt, "CREATE TABLE GOES (STUDENT_ID INT REFERENCES STUDENT(STUDENT_ID) , "
					+ "CLASS_GROUP_ID INT REFERENCES CLASS_GROUP(CLASS_GROUP_ID)  )", "GOES");
			createNewTable(stmt, "CREATE TABLE PRIMARY_KEY_COUNTERS (ID INT PRIMARY KEY, STUDENT_COUNTER INT, "
					+ "TEACHER_COUNTER INT, CLASS_GROUP_COUNTER INT , MODULE_GRADE_COUNTER INT)", "PRIMARY_KEY_COUNTERS");
			
			initializePrimaryKeyCounters(stmt);
				
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}
	
	/**
	 * This method initialize the primary keys if there are not created allready
	 * @param stmt
	 */
	private void initializePrimaryKeyCounters(Statement stmt) {
		try {
			ResultSet rs = stmt.executeQuery("SELECT * FROM PRIMARY_KEY_COUNTERS");
			boolean isAlreadyInitialized = false;
			while(rs.next())
			{
				isAlreadyInitialized = true;
				
			}
			
			if( !isAlreadyInitialized)
			{
				stmt.executeUpdate("INSERT INTO PRIMARY_KEY_COUNTERS VALUES( 0,0,0,0,0)");
				System.out.println("Inserting default values");
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}




	/**
	 * Method used to create a new table
	 * @param theStatement
	 * @param query, query to execute
	 * @param tableName, table name
	 */
	private void createNewTable(Statement theStatement, String query, String tableName)
	{
		try {
				theStatement.executeUpdate(query);

			    System.out.println("Created Table " + tableName );	
			}catch(SQLException e)
			{
				System.out.println(e.getMessage());
			}
	}
	
	
	@Override
	public int getNextPrimaryKey( int columnIndex)
	{
		try {
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery("SELECT * FROM PRIMARY_KEY_COUNTERS");
			rs.next();
			int nextPrimaryKey = rs.getInt(columnIndex);
			stmt.executeUpdate("UPDATE  PRIMARY_KEY_COUNTERS SET " + rs.getMetaData().getColumnName(columnIndex) + " = " + (nextPrimaryKey+1) );
			return nextPrimaryKey;
		
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return -1;
		}
		
	}

	@Override
	public  void addData(Object objectToAdd) 
	{
		try {

		PreparedStatement pstmt;
		String emailAll;
		String[] emailPart1;
		String[] emailPart2;
		
		switch (objectToAdd.getClass().getName())
		{	
			case "logic.Student":
				System.out.println("This is a student object");
				Student tempStudent = (Student) objectToAdd;
				pstmt = conn.prepareStatement("INSERT INTO STUDENT VALUES( ?, ?, ?, ?,?,?,?,?,?)");				
				pstmt.setInt(1, tempStudent.getStudentIdentifier());
				pstmt.setString(2, tempStudent.getName().getFirstName());
				pstmt.setString(3, tempStudent.getName().getMiddleName());
				pstmt.setString(4, tempStudent.getName().getLastName());
				pstmt.setInt(5, tempStudent.getPhone());
				pstmt.setString(9, tempStudent.getStudentDob());
				
				emailAll = tempStudent.getEmail();	
				emailPart1 =emailAll.split("@");
				pstmt.setString(6, emailPart1[0]);
				
				emailPart2 = emailPart1[1].split("\\.");
				pstmt.setString(7, emailPart2[0]);
				pstmt.setString(8, emailPart2[1]);
				pstmt.execute();				
				
				if( tempStudent.getModules().size()>0)
					addModuleRelations(tempStudent);
				
				
				
				break;
		
			case "logic.Teacher":
				System.out.println("This is a teacher object");
			
				Teacher tempTeacher = (Teacher) objectToAdd;
				pstmt = conn.prepareStatement("INSERT INTO TEACHER VALUES(?, ?, ?, ?,?,?,? ,?,?)");
				pstmt.setString(2, tempTeacher.getName().getFirstName());
				pstmt.setString(3, tempTeacher.getName().getMiddleName());
				pstmt.setString(4, tempTeacher.getName().getLastName());
				pstmt.setInt(1, tempTeacher.getTeacherIdentifier());
				pstmt.setInt(5, tempTeacher.getPhone() );
				pstmt.setString(9, tempTeacher.getDegreeQualification());
				
				emailAll = tempTeacher.getEmail();	
				emailPart1 =emailAll.split("@");
				pstmt.setString(6, emailPart1[0]);
				
				emailPart2 = emailPart1[1].split("\\.");
				pstmt.setString(7, emailPart2[0]);
				pstmt.setString(8, emailPart2[1]);
				pstmt.execute();
				
				break;
			case "logic.ClassGroup":
				System.out.println("This is a class-group object");
				
				ClassGroup tempClass = (ClassGroup) objectToAdd;
				System.out.println("Class while adding: " + tempClass.getClassIdentifier());
				pstmt = conn.prepareStatement("INSERT INTO CLASS_GROUP VALUES(?, ?)");
				pstmt.setInt(1, tempClass.getClassIdentifier());
				pstmt.setString(2 , tempClass.getClassName());
				pstmt.execute();
				
				break;
		
		}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}



	/**
	 * This method adds a relation between a module and a student
	 * @param tempStudent, student object
	 */
	private void addModuleRelations(Student tempStudent) 
	{
		for(int i = 0; i< tempStudent.getModules().size(); i++)
		{
			ModuleGrade tempModule = tempStudent.getModules().get(i);
			try {
				PreparedStatement pstmt = conn.prepareStatement("INSERT INTO  MODULE_GRADE VALUES(?, ?, ?)");
				pstmt.setInt(1, tempModule.getModuleIdentifier());
				pstmt.setString(2, tempModule.getModuleName());
				pstmt.setInt(3, tempModule.getGrade());
				pstmt.execute();
				
				pstmt = conn.prepareStatement("INSERT INTO STUDIES VALUES(?, ?)");
				pstmt.setInt(1, tempStudent.getStudentIdentifier());
				pstmt.setInt(2, tempModule.getModuleIdentifier());
				pstmt.execute();
				
			}catch(SQLException e)
			{
				System.out.println(e.getMessage());
			}
			
			
		}
	}
	
	@Override
	public void modifyGrade(ModuleGrade module, int newGrade)
	{
		try {
			PreparedStatement pstmt = conn.prepareStatement(""
					+ "UPDATE  MODULE_GRADE "
					+ "SET GRADE = ? "
					+ "WHERE MODULE_ID = ?");
			
			pstmt.setInt(1, newGrade);
			pstmt.setInt(2, module.getModuleIdentifier());
			pstmt.execute();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.out.println(e.getMessage());
		}
		
		
		
	}
	
	




	@Override
	public void addStudentClassRelation(int studentIdentifier, int classIdentifier) {
		try {
			
			System.out.println("Student: "+studentIdentifier+ " Class: "+ classIdentifier);
			PreparedStatement pstmt = conn.prepareStatement("INSERT INTO GOES VALUES(?, ?)");
			pstmt.setInt(1, studentIdentifier);
			pstmt.setInt(2, classIdentifier);
			pstmt.execute();
			
		}catch(SQLException e)
		{
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
		
	}




	@Override
	public List<ClassGroup> getExistingGroups() {
	
		ArrayList<ClassGroup> groupsFound = new ArrayList();
		try {
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery("Select * from CLASS_GROUP");
			while(rs.next())
			{
				
					int key = rs.getInt(1);
					String groupName = rs.getString(2);
					ArrayList<Student> studentsThatGo = new ArrayList<>();
					Statement stmtStudent = conn.createStatement();
					ResultSet rs2 = stmtStudent.executeQuery( 
							"SELECT  s.* "
							+ "FROM GOES g , STUDENT s "
							+ "WHERE g.CLASS_GROUP_ID = "+ key + " and s.STUDENT_ID = g.STUDENT_ID");
					
				
				
					while(rs2.next())
					{
							int keyStudent = rs2.getInt(1);
							Name theName = new Name(rs2.getString(2), rs2.getString(3), rs2.getString(4));
							String theEmail = rs2.getString(6) +rs2.getString(7)+rs2.getString(8);
							String theDOB = rs2.getString(9);
							int thePhone = rs2.getInt(5);
							Statement stmtModule = conn.createStatement();
							ResultSet rs3 = stmtModule.executeQuery(
									"SELECT  m.*"
									+ " FROM STUDIES s ,  MODULE_GRADE m "
									+ "WHERE s.STUDENT_ID = "+ keyStudent +" and s.MODULE_ID = m.MODULE_ID");
									

							
							ArrayList<ModuleGrade> moduleList = new ArrayList<>();
									
							while(rs3.next())
							{
								moduleList.add(new ModuleGrade(rs3.getInt(1), rs3.getString(2), rs3.getInt(3)));
								
							}
							
							studentsThatGo.add(new Student(keyStudent, theName , theEmail, thePhone, theDOB, moduleList));
							
							
					}
					
					groupsFound.add(new ClassGroup(key , groupName, studentsThatGo));
				
			}
			
			

		}catch(SQLException e)
		{
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
		
		
		
		return groupsFound;
	}

	@Override
	public List<Teacher> getTeacherList() {
		ArrayList<Teacher> allTeachers = new ArrayList<>();
		try {
		Statement stmt = conn.createStatement();
		ResultSet rs = stmt.executeQuery("SELECT * FROM TEACHER");
		
		
		while(rs.next())
		{
			
			//(TEACHER_ID INT PRIMARY KEY, FIRST_NAME VARCHAR(12), " + 
			//"MIDDLE_NAME VARCHAR(12), LAST_NAME VARCHAR(12), PHONE INT,EMAILBODY VARCHAR(50), EMAILCOMPANY VARCHAR(50), EMAILTERMINATION VARCHAR(10), "
			//+ "DEGREE_QUALIFICATION VARCHAR(24))
		
			
			int keyTeacher = rs.getInt(1);
			Name theName = new Name(rs.getString(2), rs.getString(3), rs.getString(4));
			String theEmail = rs.getString(6) +rs.getString(7)+rs.getString(8);
			String DegreeQualification = rs.getString(9);
			int thePhone = rs.getInt(5);
			allTeachers.add(new Teacher( keyTeacher, theName, theEmail, thePhone, DegreeQualification       ));
		}
		
		
		}catch(SQLException e)
		{
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
		
		
		
		return allTeachers;
	}

	
	@Override
	public void deleteStudent(Student student, boolean isInClass)
	{
		
		//"CREATE TABLE STUDIES (STUDENT_ID INT REFERENCES STUDENT(STUDENT_ID), "
		//		+ "MODULE_ID INT REFERENCES MODULE_GRADE(MODULE_ID) )"
		try {
	
			Statement stmt = conn.createStatement();
			stmt.executeUpdate("DELETE FROM STUDIES WHERE STUDENT_ID = " + student.getStudentIdentifier());
			if(isInClass)
				stmt.executeUpdate("DELETE FROM GOES WHERE STUDENT_ID  = " + student.getStudentIdentifier());
			
			stmt.executeUpdate("DELETE FROM STUDENT WHERE STUDENT_ID = " + student.getStudentIdentifier());
			
			student.getModules().forEach(x -> {
				try {
					stmt.executeUpdate("DELETE FROM MODULE_GRADE WHERE MODULE_ID = "+ x.getModuleIdentifier());
				} catch (SQLException e) {
					System.out.println(e.getMessage());
				}
			} );
			
		}catch(SQLException e)
		{
			System.out.println(e.getMessage());
		}
		
	}
	
	
	@Override
	public void deleteClassGroup(ClassGroup classGroup) {
		try {
			Statement stmt = conn.createStatement();
			stmt.executeUpdate("DELETE FROM GOES WHERE CLASS_GROUP_ID = " + classGroup.getClassIdentifier());
			stmt.executeUpdate("DELETE FROM CLASS_GROUP WHERE CLASS_GROUP_ID = " + classGroup.getClassIdentifier());
			classGroup.getClassGroup().forEach(x -> deleteStudent(x, false));
		}catch(SQLException e)
		{
			System.out.println(e.getMessage());
		}
		
		
	}

}
