package logic;

import java.util.ArrayList;
import java.util.List;

/**
 * Class used to represt a class of the database
 * @author R00171724
 *
 */
public class ClassGroup 
{
	private String className ;
	private List<Student> classGroup;
	private int classIdentifier;
	/**
	 * Constructor of the class
	 * @param classIdentifier, primary key of the database table
	 * @param className, string name of the class
	 */
	public ClassGroup(int classIdentifier, String className) {
		this.className = className;
		classGroup = new ArrayList<Student>(); 
		this.classIdentifier = classIdentifier;
	}
	
	/**
	 * Getter of primary key
	 * @return int primary key
	 */
	public int getClassIdentifier() {
		return classIdentifier;
	}

	/**
	 * Another constructor of the claas
	 * @param classIdentifier, priamry key
	 * @param className, the class name
	 * @param classGroup, list of students in that class
	 */
	public ClassGroup(int  classIdentifier, String className,List<Student> classGroup)
	{
		this.className = className;
		this.classGroup = classGroup;
		this.classIdentifier = classIdentifier;
	}
	
	/**
	 * getter of the class name
	 * @return string of the class name
	 */
	public String getClassName() {
		return className;
	}
	
	/**
	 * setter of the class name
	 * @param className, stirng of the class naem
	 */
	public void setClassName(String className) {
		this.className = className;
	}
	/**
	 * getter of the class group students
	 * @return list with all the students
	 */
	public List<Student> getClassGroup() {
		return classGroup;
	}
	
	/**
	 * Setter of the student list
	 * @param classGroup, the new list with the students
	 */
	public void setClassGroup(List<Student> classGroup) {
		this.classGroup = classGroup;
	}
	
	
	@Override
	public String toString() {
		return getClassName() +" - nº student: " + classGroup.size();
	}
	
	

}
