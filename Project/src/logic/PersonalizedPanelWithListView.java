package logic;

public interface PersonalizedPanelWithListView
{
	/**
	 * This method set up the specific control object of each setting up panel class
	 * @param <T> type of the object to setup
 	 * @param specificObject, object to setup
	 */
	<T> void setUpSpecificObject(T specificObject);
	/**
	 * Method that refresh the listview of the class
	 */
	void refreshListView();

}
