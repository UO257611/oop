package logic;

/**
 * Teacher class representation 
 * @author R00171724
 *
 */
public class Teacher extends Person
{
	private String degreeQualification;
	private int teacherIdentifier;
	
	/**
	 * General constructor
	 * @param teacherIdentifier, inr primary key of the object
	 * @param name, 
	 * @param email,
	 * @param phone
	 * @param degreeQualification
	 */
	public Teacher(int teacherIdentifier,Name name, String email, int phone, String degreeQualification) {
		super(name, email, phone);
		this.degreeQualification = degreeQualification;
		this.teacherIdentifier = teacherIdentifier;
	}

	/**
	 * getter of the teacher primary key
	 * @return int teacher primary key
	 */
	public int getTeacherIdentifier() {
		return teacherIdentifier;
	}

	/**
	 * Getter of the degree qualification
	 * @return string the degree name
	 */
	public String getDegreeQualification() {
		return degreeQualification;
	}
	
	/**
	 * Setter of the degree qualification
	 * @param degreeQualification
	 */
	public void setDegreeQualification(String degreeQualification) {
		this.degreeQualification = degreeQualification;
	}

	@Override
	public String toString() {
		return getName().getFirstName() + " " +getName().getMiddleName() +" "+ getName().getLastName() +  " - " + getDegreeQualification();
	}
	
	

}
