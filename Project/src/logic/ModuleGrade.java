package logic;

/**
 * 
 * Class that represent a module in the database
 * @author R00171724
 *
 */
public class ModuleGrade {
	
	private String moduleName;
	private int grade;
	private int moduleIdentifier;
	
	/**
	 * This method return the module name 
	 * @return modulename
	 */
	public String getModuleName() {
		return moduleName;
	}

	/**
	 * This method set a new grade in the module
	 * @param grade, int new grade
	 */
	public void setGrade(int grade) {
		this.grade = grade;
	}

	/**
	 * This method return the primary key of the object
	 * @return int primary key
	 */
	public int getModuleIdentifier() {
		return moduleIdentifier;
	}

	/**
	 * This method return the grade of the module
	 * @return int the grade
	 */
	public int getGrade() {
		return grade;
	}

	/**
	 * This is the constructor  of the class
	 * @param moduleIdentifier, int priamry key
	 * @param moduleName, string module name
	 * @param grade, int grade of the module
	 */
	public ModuleGrade(int moduleIdentifier, String moduleName, int grade) {
		this.moduleName = moduleName;
		this.grade = grade;
		this.moduleIdentifier = moduleIdentifier;
	}
	
	@Override
	public String toString() {
		return moduleName+ ":" + grade;
	}

}
