package logic;
/**
 * This is the class for the name, demanded in the pdf
 * @author R00171724
 *
 */
public class Name 
{
	
	private String firstName;
	private String middleName;
	private String lastName;
	
	/**
	 * Constructor of the class
	 * @param firstName, String firstname
	 * @param middleName, String middle name
	 * @param lastName, string lastname
	 */
	public Name(String firstName, String middleName, String lastName) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.middleName = middleName;
		
	}
	
	/**
	 * getter of the firstname 
	 * @return stirng firstname
	 */
	public String getFirstName() {
		return firstName;
	}
	/**
	 * setter of the first name
	 * @param firstName, string first name
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	
	/**
	 * getter of the middle name 
	 * @return string middle name
	 */
	public String getMiddleName() {
		return middleName;
	}
	
	/**
	 * Setter of the middle name
	 * @param middleName
	 */
	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}
	
	/**
	 * Getter of the last name
	 * @return string last name
	 */
	public String getLastName() {
		return lastName;
	}
	
	/**
	 * setter of the lastname
	 * @param lastName
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	

}
