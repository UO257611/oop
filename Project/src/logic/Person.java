package logic;

/**
 * Class that represents a persons
 * @author R00171724
 *
 */
public class Person 
{
	private Name name;
	private String email;
	private int phone;
	
	/**
	 * General constructor
	 * @param name
	 * @param email
	 * @param phone
	 */
	public Person(Name name, String email, int phone)
	{
		this.name = name;
		this.email = email;
		this.phone = phone;
		
	}
	
	/**
	 * Getter of the Name
	 * @return Name objects
	 */
	public Name getName() {
		return name;
	}
	/**
	 * Getter of the email 
	 * @return String of the email
	 */
	public String getEmail() {
		return email;
	}
	
	/**
	 * Getter of the phone
	 * @return int representing the phone
	 */
	public int getPhone() {
		return phone;
	}
	
	/**
	 * Setter for the phone
	 * @param number
	 */
	public void setPhone(int number) {
		this.phone = number;
	}
	
	/**
	 * Return the full name
	 * @return String full name
	 */
	public String getFullName()
	{
		return name.getFirstName() +" "+ name.getMiddleName() +" "+ name.getLastName();
	}
	

}
