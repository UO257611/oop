package logic;

import java.util.List;

public interface DatabaseInterface
{

	static final int STUDENT_KEY_COLUMN = 1;
    static final int TEACHER_KEY_COLUMN = 2;
    static final int CLASS_GROUP_KEY_COLUMN = 3;
    static final int MODULE_KEY_COLUMN = 4;
    
    /**
     * This method  that adds new data to the database
     * @param objectToAdd, object to add
     */
	void addData (Object objectToAdd );
	/**
	 * This method returns the next primary key of a specifc table 
	 * @param column, column that identifies the object
	 * @return int the next primary key
	 */
	int  getNextPrimaryKey(int column);
	/**
	 * This method adds a relation bewtween a student and a class
	 * @param studentIdentifier, student primary key
	 * @param classIdentifier, class group primary key
	 */
	void addStudentClassRelation(int studentIdentifier, int classIdentifier);
	/**
	 * getter of all the existing class gorups in the database
	 * @return list with all the classgroups
	 */
	List<ClassGroup> getExistingGroups();
	/**
	 * This method changes the grade of a module
	 * @param module, module object 
	 * @param newGrade, int with the new grade
	 */
	void modifyGrade(ModuleGrade module, int newGrade);
	/**
	 * Getter of all the teachers of the database
	 * @return list with all the teachers in the database
	 */
	List<Teacher> getTeacherList();
	/**
	 * This method removes a classgroup from the database
	 * @param classGroup, class group object
	 */
	void deleteClassGroup(ClassGroup classGroup);
	/**
	 * This method removes a student of a class
	 * @param student, student to be remove
	 * @param isDeletedInClass, boolean that checks if is deleted in the class
	 */
	void deleteStudent(Student student, boolean isDeletedInClass);

}
