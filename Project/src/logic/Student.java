package logic;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collector;
import java.util.stream.Collectors;

/**
 * A class that represents a student
 * @author R00171724
 *
 */
public class Student extends Person
{
	
	private int studentIdentifier;
	private String studentDob; //This means date of birth
	private List<ModuleGrade> modules;
	/**
	 * General constructor
	 * @param studentIdentifier
	 * @param name
	 * @param email
	 * @param phone
	 * @param studentDob
	 * @param modules
	 */
	public Student(int studentIdentifier, Name name, String email, int phone, String studentDob, List<ModuleGrade> modules)
	{
		super(name, email, phone);
		this.studentDob = studentDob;
		this.modules = modules;
		this.studentIdentifier = studentIdentifier;
	}
	/**
	 * Empty object constructor
	 */
	public Student() {
		super(null,"", 0);
		
	}
	
	/**
	 * getter of the primary key of the object
	 * @return int primary key
	 */
	public int getStudentIdentifier() {
		return studentIdentifier;
	}
	
	/**
	 * Getter of the dob
	 * @return string dob
	 */
	public String getStudentDob() {
		return studentDob;
	}
	
	/**
	 * Getter of the module list
	 * @return list with the module objects
	 */
	public List<ModuleGrade> getModules() {
		return modules;
	}
	
	/**
	 * setter of the module list
	 * @param modules
	 */
	public void setModules(List<ModuleGrade> modules) {
		this.modules = modules;
	}
	
	/**
	 * method that adds a module to the module lsit
	 * @param moduleIdentifier, module primary key
	 * @param moduleName, module string name
	 * @param gradeName, int grade
	 */
	public void addModule(int moduleIdentifier, String moduleName, int gradeName) {
		if(modules.size() <6)
			modules.add(new ModuleGrade(moduleIdentifier , moduleName, gradeName));
		
	}
	
	/**
	 * Removes a module from the module list
	 * @param moduleName, name of the module
	 */
	public void removeModule(String moduleName) {
		
		modules = modules.stream().filter(x -> x.getModuleName() == moduleName ).collect(Collectors.toList());
	}
	
	@Override
	public String toString() {
		String x = getName().getFirstName() + " " +getName().getMiddleName() +" "+ getName().getLastName() + " -> " ;
		for(ModuleGrade module: modules)
		{
			x += module +" ";
		}
		return x;
		
		
	}

}
