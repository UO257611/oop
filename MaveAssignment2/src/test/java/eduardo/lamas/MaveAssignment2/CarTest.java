package eduardo.lamas.MaveAssignment2;

import static org.junit.jupiter.api.Assertions.*;


import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import eduardo.lamas.logic.Car;

class CarTest {

	@Test
	public void testCarCreation() {
		Car theCar = new Car("Prius", 59302, 1000, 2007, "Toyota");
		assertEquals("Prius", theCar.getModel());
		assertEquals("Toyota", theCar.getMaker());
		assertEquals(59302, theCar.getRegistrationNumber());
		assertEquals(theCar.getMillage(), 1000);
		assertEquals(theCar.getYearOfManufacture(), 2007);

	}

}
