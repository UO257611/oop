package eduardo.lamas.MaveAssignment2;

import static org.junit.jupiter.api.Assertions.*;

import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import eduardo.lamas.logic.Car;
import eduardo.lamas.logic.CarList;

class CarListTest {

	CarList carList;
	Car car1;
	Car car2;
	@BeforeEach
	void setUp() throws Exception {
		carList = new CarList();
		car1 = new Car("Prius", 59302, 1000, 2007, "Toyota");
		car2 = new Car("Carrera", 46851, 200, 1998, "Porche");
	}

	@Test
	public void testAdd() {
		
		carList.add(car1);
		carList.add(car2);
		
		assertEquals(2, carList.getSize());
		List<Car> tempList = carList.listAllCars();
		
		assertEquals(tempList.get(0), car1);
		assertEquals(tempList.get(1), car2);
		
	}
	
	@Test
	public void testRemove() {

		carList.add(car1);
		carList.add(car2);
		
		assertEquals(2, carList.getSize());
		
		carList.remove(car1);
		List<Car> tempList = carList.listAllCars();
		
		assertEquals(tempList.get(0), car2);
	}
	
	@Test
	public void testUnmodifiableListCars() {
		carList.add(car1);
		carList.add(car2);
		
		assertEquals(2, carList.getSize());
		
		List<Car> tempList = carList.listAllCars();
		try {
			tempList.add(new Car("akfh", 1, 1, 1, "afhkasj"));
			fail("The list must be unmodifiable");
		}catch (Exception e) {
			assertTrue(true);
		}
		
		
	}

}
