package eduardo.lamas.MaveAssignment2;
	


import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

import javax.swing.event.ChangeEvent;

import eduardo.lamas.logic.Car;
import eduardo.lamas.logic.CarList;
import eduardo.lamas.logic.ReportGenerator;
import javafx.application.Application;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.layout.Border;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
/**
 * 
 * This is the main class of the javafx framework
 * 
 * @author Eduardo Lamas Suárez
 *
 */
public class Main extends Application {
	
	
	//Implementation of an arrayList
	private CarList listOfCars = new CarList();
	@Override
	/**
	 * Method that is called when the program is launch that set up
	 * the window panes
	 */
	public void start(Stage primaryStage) {
		try {

			BorderPane root = new BorderPane();
			Scene scene = new Scene(root,400,400);
			scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
			primaryStage.setScene(scene);
			
			loadCars();
			
			settupSerialization(primaryStage);
			
			root.setCenter(setUpGeneralWindow(root));
			
			
			primaryStage.show();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}






	/**
	 * This method loads the saved car list from previous sessions
	 */
	private void loadCars() 
	{
		try {
			System.out.println("Loading data");
			FileInputStream fileIn = new FileInputStream("Files/CarList.ser");
			
			
			ObjectInputStream in = new ObjectInputStream(fileIn);
			CarList tempCarList = (CarList)in.readObject();
			listOfCars = tempCarList;
			in.close();	
			fileIn.close();
			System.out.println("Data Loaded");
		}catch (Exception e) {
			System.out.println("Error while loading the data");
			System.out.println("Creating an empty car list, in order to not break");
			listOfCars = new CarList();
			
		}
	}






	/**
	 * This method set up the serialization process for saving the state
	 * of the arrayList of cars when closing the program
	 * @param primaryStage, stage of the closing window 
	 */
	private void settupSerialization(Stage primaryStage) {
		primaryStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
			
			@Override
			public void handle(WindowEvent event) {
				System.out.println("Size: " + listOfCars.getSize());	
				try {
					System.out.println("Saving data");
					FileOutputStream fileOut = new FileOutputStream("Files/CarList.ser");
					ObjectOutputStream out = new ObjectOutputStream(fileOut);
					out.writeObject(listOfCars);
					out.close();
					fileOut.close();
						System.out.println("Data saved");
				}catch (Exception e) {
					System.out.println("Error while saving the data");
				}
			
			
			}
		});
	}
	
	

	



	/**
	 * Method used to setup the general window
	 * @param root, the root layout (panel)
	 * @return the final pane to insert inside the center of root border pane.
	 */
	private VBox setUpGeneralWindow(BorderPane root) 
	{
		
		//Local variables
		BorderPane pnGeneral = new BorderPane();
		VBox pnVertical = new VBox();
		VBox pnAddCarVertical = new VBox();
		HBox pnAddCarGeneralHorizontal = new HBox();
		HBox pnHorizontalRow1  = new HBox();
		HBox pnHorizontalRow2  = new HBox();
		HBox pnHorizontalRow3 = new HBox();
		HBox pnReports = new HBox();
		ListView<Car> lsCar1 = new ListView<Car>();
		ObservableList<Car> observableCarList = FXCollections.observableArrayList();
		lsCar1.setItems(observableCarList);	
		Button btnAdd = new Button("Add");
		Button btnRemove = new Button("Remove");
		Button btnNaturalReport = new Button("Natural");
		Button btnYearReport = new Button("Year");
		Button btnMillageReport = new Button("Millage");
		Label lblMaker = new Label("Maker:");
		Label lblModel = new Label("Model:");
		Label lblRegistrationNumber = new Label("Registration Number:");
		Label lblMillage = new Label("Millage:");
		Label lblYear = new Label("Year:");
		Label lblCarList = new Label("Car List");
		Label lblReports = new Label("Report Options");
		TextField txtMaker = new TextField();
		TextField txtModel = new TextField();
		TextField txtRegistration = new TextField();
		TextField txtMillage = new TextField();
		TextField txtYear = new TextField();

		// Set the Max size of some elemetns
		txtMaker.setMaxSize(100, 100);
		txtModel.setMaxSize(100, 100);
		txtYear.setMaxSize(100, 100);
		txtRegistration.setMaxSize(100, 100);
		txtMillage.setMaxSize(100, 100);
		
		lblCarList.setFont(new Font(30));
		lblCarList.setUnderline(true);
		lblReports.setFont(new Font(30));
		lblReports.setUnderline(true);

		lsCar1.setPrefSize(400, 300);
		lsCar1.setMaxWidth(400);
		setUpList(lsCar1);
		
		btnAdd.setPrefSize(80, 70);
	
		//Adding the elements and setting them in the panes
		pnHorizontalRow1.getChildren().addAll(lblMaker, txtMaker,lblModel,txtModel);
		pnHorizontalRow2.getChildren().addAll(lblYear, txtYear,lblMillage,txtMillage);
		pnHorizontalRow3.getChildren().addAll(lblRegistrationNumber, txtRegistration);
		pnHorizontalRow1.setAlignment(Pos.CENTER);
		pnHorizontalRow2.setAlignment(Pos.CENTER);;
		pnHorizontalRow3.setAlignment(Pos.CENTER);
		pnHorizontalRow1.setSpacing(5);
		pnHorizontalRow2.setSpacing(5);
		pnHorizontalRow3.setSpacing(5);
		
		pnReports.getChildren().addAll(btnNaturalReport, btnYearReport,btnMillageReport);
		pnReports.setAlignment(Pos.CENTER);
		pnReports.setSpacing(20);
		
		pnAddCarGeneralHorizontal.setAlignment(Pos.CENTER);
		pnAddCarGeneralHorizontal.getChildren().addAll(pnAddCarVertical,btnAdd);
		pnAddCarGeneralHorizontal.setSpacing(10);
		
		pnAddCarVertical.setAlignment(Pos.CENTER);
		pnAddCarVertical.getChildren().addAll(pnHorizontalRow1,pnHorizontalRow2,pnHorizontalRow3);
	
		pnVertical.getChildren().addAll(pnAddCarGeneralHorizontal,lblCarList,lsCar1,btnRemove,lblReports,pnReports);
		pnVertical.setAlignment(Pos.CENTER);
		pnVertical.setSpacing(20);
		
		//Setting the Action listener to the buttons
		setButtonListeners(lsCar1, btnAdd, btnRemove, btnNaturalReport, btnYearReport, btnMillageReport, txtMaker,
				txtModel, txtRegistration, txtMillage, txtYear);
		
		lsCar1.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Car>() {

			@Override
			public void changed(ObservableValue<? extends Car> observable, Car oldValue, Car newValue) {	
				if (newValue != null) {
					btnRemove.setDisable(false);
				}else {
					btnRemove.setDisable(true);
				}
			}});
		
		
		return pnVertical;
		
		
	}





	/**
	 *  Method where all the buttons listener are set up
	 * @param lsCar1, The listview of the window
	 * @param btnAdd , button that adds new cars
	 * @param btnRemove, button that removes a car 
	 * @param btnNaturalReport, button that generate the report with a natural sort method
	 * @param btnYearReport, button that generate the report with a year sort method
	 * @param btnMillageReport, utton that generate the report with a millage sort method
	 * @param txtMaker, textfield where is the user input written
	 * @param txtModel, textfield where is the user input written
	 * @param txtRegistration, textfield where is the user input written
	 * @param txtMillage, textfield where is the user input written
	 * @param txtYear, textfield where is the user input written
	 */
	private void setButtonListeners(ListView<Car> lsCar1, Button btnAdd, Button btnRemove, Button btnNaturalReport,
			Button btnYearReport, Button btnMillageReport, TextField txtMaker, TextField txtModel,
			TextField txtRegistration, TextField txtMillage, TextField txtYear)
	{
		
		btnNaturalReport.setOnAction( x -> ReportGenerator.naturalReport(listOfCars));
		
		btnYearReport.setOnAction(x -> ReportGenerator.yearReport(listOfCars));
		
		btnMillageReport.setOnAction(x -> ReportGenerator.millageReport(listOfCars));
		
		
		btnAdd.setOnAction(x -> {
			Car tempCar = new Car(txtModel.getText(), Integer.parseInt(txtRegistration.getText()), 
					Integer.parseInt(txtMillage.getText()), Integer.parseInt(txtYear.getText()), txtMaker.getText());
			lsCar1.getItems().add(tempCar);
			listOfCars.add(tempCar);
			
		});
		
		btnRemove.setOnAction(x -> {
			Car tempCar = lsCar1.getSelectionModel().getSelectedItem();
			listOfCars.remove(tempCar);
			lsCar1.getItems().remove(tempCar);
			lsCar1.refresh();
		});
		
		btnRemove.setDisable(true);
	}
	
	
	/**
	 * This method adds all the elemetns of our implementation of the car list into a listview
	 * @param lsCar, listview of the winodow, to show the list
	 */
	private void setUpList(ListView lsCar) {
		if(listOfCars.getSize() > 0) {
			
			listOfCars.listAllCars().forEach(car -> lsCar.getItems().add(car));
			lsCar.refresh();
		}
		
	}



	/**
	 * Main method of the application
	 * @param args, array of strings
	 */

	public static void main(String[] args) {
		launch(args);
	}
}
