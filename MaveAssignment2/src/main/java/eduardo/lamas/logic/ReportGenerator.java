package eduardo.lamas.logic;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class ReportGenerator
{
	/**
	 * This method creates a report with all cars sorted in a natural way
	 * @param listOfCars, list of cars
	 */
	public static void naturalReport(CarList listOfCars) 
	{
		ArrayList<Car> cars = new ArrayList<>();
		listOfCars.listAllCars().forEach(car -> cars.add(car));
		Collections.sort(cars, Comparator.comparing(Car::getMaker).
				thenComparing(Car::getModel));
		writeIntoFile(cars,"NaturalReport");
		
	}

	/**
	 * This method writes a list into a file
	 * @param cars, list of the cars that is already sorted
	 * @param Filename, filename
	 */
	private static void writeIntoFile(ArrayList<Car> cars, String Filename) {
		try {
			BufferedWriter writer = new BufferedWriter( new FileWriter("Files/"+Filename+".txt"));
			cars.forEach(car -> {
				try {
					writer.write(car.toString() +"\n");
				} catch (IOException e) {
					e.printStackTrace();
				}
			});
			writer.close();
		}catch(Exception e) {
			
			System.out.println("Error while writting to the natural report file");
		}
	}
	
	/**
	 * This method creates a report with all cars sorted by the year
	 * @param listOfCars, list of cars
	 */
	public static void yearReport(CarList listOfCars) {
		ArrayList<Car> cars = new ArrayList<>();
		listOfCars.listAllCars().forEach(car -> cars.add(car));
		Collections.sort(cars, Comparator.comparing(Car::getYearOfManufacture));
		writeIntoFile(cars,"YearReport");
		
		
	}
	
	/**
	 * This method creates a report with all cars sorted by the millages
	 * @param listOfCars, list of cars
	 */
	public static void millageReport(CarList listOfCars) {
		ArrayList<Car> cars = new ArrayList<>();
		listOfCars.listAllCars().forEach(car -> cars.add(car));
		Collections.sort(cars, Comparator.comparing(Car::getMillage));
		writeIntoFile(cars,"MillagelReport");
		
	}
	
	

}
