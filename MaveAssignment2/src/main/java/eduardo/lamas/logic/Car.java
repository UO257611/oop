package eduardo.lamas.logic;

import simpleCar.SimpleCar;

/**
 * 
 * This class is a representation of a car
 * 
 * @author Eduardo Lamas Suárez
 *
 */
public class Car extends SimpleCar implements java.io.Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int millage ;
	
	/**
	 * Constructor the object
	 * @param model, model of the car
	 * @param registrationNumber, registration number of the car
	 * @param millage, millage of the car
	 * @param yearOfManufacture, year of the creation of teh car
	 * @param maker, maker of the car
	 */
	public Car(String model, int registrationNumber, int millage, int yearOfManufacture , String maker) {
		super(model, registrationNumber, yearOfManufacture, maker);
		this.millage = millage;
	}
	
	


	/**
	 * Method that return the millage of the car
	 * @return the millage of the car
	 */
	public int getMillage() {
		return millage;
	}

	/**
	 * ToString method
	 */
	@Override
	public String toString() {
		return "" + getMaker()+" - " + getModel()+" - " + getYearOfManufacture()+" - " + getMillage() +" - "+ String.valueOf(getRegistrationNumber()); 
	}

}
