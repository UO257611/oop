package logic;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.swing.event.ListSelectionEvent;


/**
 * 
 * @author Eduardo Lamas Suárez
 * This class is our own representation of the arrayList class
 * This class used the encapsulation technique, in order to have some control
 * on the data that access the arraylist
 */
public class CarList implements java.io.Serializable
{
	private static final long serialVersionUID = 1L;
	private ArrayList<Car> listOfCars ;
	
	/**
	 * Constructor the class that it doesn't need anything, creates an empty list
	 */
	public CarList() {
		listOfCars = new ArrayList<>();
	}
	
	/**
	 * Constructor that creates the arraylist from another list
	 * @param tempCarList,  the carlist used to create the new one
	 */
	public CarList(List<Car> tempCarList) {
		
		listOfCars = new ArrayList<>();
		tempCarList.forEach(x -> listOfCars.add(x));
	}
	
	/**
	 * This method return the size of the arrayList
	 * @return The size of the lsit
	 */
	public int getSize() {
		return listOfCars.size();
	}
	
	/**
	 * This method adds a new car to the list
	 * @param theCar, car that is going to be added
	 */
	public void add(Car theCar) {
		listOfCars.add(theCar);
		
	}
	
	/**
	 * This method remove a car from the list
	 * @param theCar, the car to be removed
	 */
	public void remove(Car theCar) {
		listOfCars.remove(theCar);
		
	}
	
	/**
	 * Method that list all the cars
	 * @return an unmodifiable list with all the cars
	 */
	public List<Car> listAllCars() {
		return Collections.unmodifiableList(listOfCars);
		
	}
	
	
	
	

}
