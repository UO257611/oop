package VisualVM_MemoryTest;

import java.util.ArrayList;

import logic.Car;

public class MemoryTest 
{
	
	public static void main(String[] args) {
		ArrayList<Car> cars = new ArrayList<>();
		
		/* Checking loop with an ending state
		for(int i = 0; i< 10000000; i++) {
			
			cars.add(new Car(""+i, i, i, i, ""+i));
						
		}*/
		int i = 0;
		while (true) {
			cars.add(new Car(""+i, i, i, i, ""+i));
			i++;
		}
		//System.out.println("Finished");
	}
	
	
	
}
