package simpleCar;

import java.io.Serializable;

/**
 * 
 * @author Eduardo Lamas Suárez
 *
 */
public class SimpleCar implements Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String model;
	private int registrationNumber;
	private int yearOfManufacture;
	private String maker;
	
	/**
	 * Constructor of the class
	 * @param model
	 * @param registrationNumber
	 * @param yearOfManufacture
	 * @param maker
	 */
	public SimpleCar(String model, int registrationNumber, int yearOfManufacture , String maker) {
		
		this.maker = maker;
		this.model = model;
		this.yearOfManufacture = yearOfManufacture;
		this.registrationNumber = registrationNumber; 
	}
	
	
	


	/**
	 * Method that return the model
	 * @return model of the car
	 */
	public String getModel() {
		return model;
	}


	/**
	 * Method that return the registration number
	 * @return registration number of the car
	 */
	public int getRegistrationNumber() {
		return registrationNumber;
	}



	/**
	 * Method that return the year of creation
	 * @return the year where the car is created
	 */
	public int getYearOfManufacture() {
		return yearOfManufacture;
	}
	
	/**
	 * To string method
	 */
	@Override
	public String toString() {
		return "" + getMaker()+" - " + getModel()+" - " + getYearOfManufacture()+" - " + String.valueOf(getRegistrationNumber()); 
	}


	/**
	 * Method that return the maker of the car
	 * @return the maker of the car
	 */
	public String getMaker() {
		return maker;
	}

}
