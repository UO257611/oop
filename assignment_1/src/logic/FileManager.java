package logic;

import java.io.BufferedReader;
import java.io.FileReader;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;

public class FileManager 
{
	
	public static HashMap<Integer, ArrayList<Prize>> ReadPrizeFile(String filename){
		HashMap<Integer,ArrayList<Prize>> prize = new HashMap<>();
		for(int i = 0; i<5; i++)
		{
			prize.put(i+1, new ArrayList<Prize>());
		}
		try {
			BufferedReader reader = new BufferedReader(new FileReader(filename));
			while(reader.ready())
			{
				String line = reader.readLine();
				String[] splittedLine = line.split("@");
				prize.get(Integer.parseInt(splittedLine[1])).add(new Prize(Integer.parseInt(splittedLine[1]), splittedLine[2], splittedLine[0]));
			}
			reader.close();
		}catch(Exception e) {
			System.out.println("Error while loading the prize");
		}
		return prize;
	}
	
	
	public static ArrayList<String> ReadHangManFile(String filename)
	{
		ArrayList<String> wordList = new ArrayList<>();
		try {
			BufferedReader reader = new BufferedReader(new FileReader(filename));
			while(reader.ready()) {
				wordList.add(reader.readLine());
			}
			reader.close();
		}catch (Exception e) {
			System.out.println("Error while loading the word file");
			
		}
		return wordList;
		
	}

}
