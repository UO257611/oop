package logic;

import java.util.function.Function;
import java.util.function.Supplier;

import javafx.scene.control.Tab;

public class NumberGuessGame  implements GameInterface<Integer>
{
	private int attemps;
	private int secretNumber;
	private boolean blockGame;
	private int winsInARow = 0;
	private Integer[] prizesCounter;
	
	
	public NumberGuessGame(Integer[] prizesCounter) {
		attemps = 0;
		this.prizesCounter = prizesCounter;
		secretNumber = NumberGeneration.getRandomGuessGameNumber();
		System.out.println("The secret guess number: "+ secretNumber);
	}

	
	public String game(Integer numberUser) {
		
		attemps++;
		Supplier< String> threeStars = ()-> {
			prizesCounter[2]++;
			return "You won a three stars prize";
		};
		Supplier<String> twoStars = () ->{
			prizesCounter[1]++;
			return "You won a two stars prize";
			
		};
		if(!blockGame)
		{
			if(attemps < 5) {
				if( numberUser > secretNumber)
					return "Your number is higher";
				else if(numberUser < secretNumber)
					return "Your number is lower";
				else
				{
					winsInARow++;
					blockGame = true;
					return winsInARow == 5? threeStars.get(): twoStars.get();
				}
				
			}else {
				winsInARow = 0;
				return "Your surpassed the number of attemps";
			}
		}else {
			return "You won already this number, please reset the game";
		}
		
	}
	
	public void reset()
	{
		attemps= 0;
		blockGame=false;
		secretNumber = NumberGeneration.getRandomGuessGameNumber();
		
		System.out.println("The secret guess number: "+ secretNumber);
		
	}


	
	
	

}
