package logic;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

import com.sun.javafx.webkit.ThemeClientImpl;

import javafx.application.Platform;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.Tab;

public class ThreadToUpdate extends Thread
{
	private Integer[] prizesWonCounter;
	private Integer[] prizesWonCounterCopy;
	private Label starsCounter;
	private ListView<Prize> prizeList;
	private HashMap<Integer, ArrayList<Prize>> prizeMap;
	private Tab prizesTab;
	
	public  ThreadToUpdate(Label starsCounter, Integer[] prizesWonCounter, ListView<Prize> theList, HashMap<Integer,ArrayList<Prize>> prizeMap, Tab prizesTab ) 
	{
		super();
		this.prizesTab = prizesTab;
		this.starsCounter = starsCounter;
		this.prizesWonCounter = prizesWonCounter;
		this.prizesWonCounterCopy = prizesWonCounter.clone();
		this.prizeList = theList;
		this.prizeMap = prizeMap;
	}
	
	
	@Override
	public void run() {
		super.run();
		
		while(true) {
			if(counterHasChange())
			{
				Platform.runLater(() -> {
					String lblStarXCounter = "";
					for(int i = 0; i< prizesWonCounter.length; i++)
					{
						lblStarXCounter += "Star "+ (i+1) +": " + prizesWonCounter[i]+" - ";
						
					}
					starsCounter.setText(lblStarXCounter);
				});
				System.out.println("A change has happen");
				prizesWonCounterCopy = prizesWonCounter.clone();
				Platform.runLater(() ->{
					prizeList.getItems().clear();
					ArrayList<Prize> allAvailableprizes = new ArrayList<>();
					for(int i = 0; i< prizesWonCounter.length; i++)
					{
						if(prizesWonCounter[i] > 0) {
							allAvailableprizes.addAll(prizeMap.get(i+1));
						}
					}
					Collections.shuffle(allAvailableprizes);
					
					prizeList.getItems().addAll(allAvailableprizes);
	
					prizeList.refresh();
				});
				prizesTab.setDisable(false);
				
			}
			
			
			
			
			
		}
		
	}
	
	private boolean counterHasChange()
	{
		
		for(int i = 0; i< prizesWonCounter.length;i++)
		{
			if(prizesWonCounter[i] != prizesWonCounterCopy[i])
				return true;
			
		} 
		return false;
	}

	
	
}
