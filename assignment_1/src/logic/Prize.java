package logic;

public class Prize 
{
	private int weight;
	private String stringName;
	private String actualPrice;
	
	public Prize(int weight, String stringName, String actualPrice) {
		
		this.weight = weight;
		this.stringName = stringName;
		this.actualPrice = actualPrice;
	}
	
	public String getRealPrize() {
		return actualPrice;
	}
	public int getWeight() {
		return weight;
	}

	@Override
	public String toString() {
		return stringName;
	}
	
	
	
	

}
