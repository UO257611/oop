package logic;

public interface GameInterface<T>
{
	void reset();
	String game(T input); 
}
