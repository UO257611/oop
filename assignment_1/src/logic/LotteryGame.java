package logic;

import java.util.ArrayList;
import java.util.Collections;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

import javafx.scene.control.Tab;

public class LotteryGame implements GameInterface<Integer[]>
{
	private ArrayList<Integer> lotteryNumbers;
	private boolean fourNumber;
	private boolean fiveNumbers;
	private Integer[] prizeCounter;
	public LotteryGame(Integer[] prize) 
	{
		
		this.prizeCounter = prize;
		
		
		lotteryNumbers = new ArrayList<Integer>();
		for(int i = 0; i < 5; i++)
		{
			lotteryNumbers.add(NumberGeneration.getRandomLotteryNumber());
			
		}
		for(int i = 0; i < lotteryNumbers.size(); i++)
		{
			System.out.print(lotteryNumbers.get(i) + "\t");
		}
		System.out.println("");
		
	}
	
	
	
	public String game( Integer[] playerNumbers)
	{
		int correctNumbers = 0;
		ArrayList<Integer> guessedNumbers = new ArrayList<>();
 		for(int i = 0; i<playerNumbers.length; i++)
		{
 			try {
	 			int x = playerNumbers[i];
	 			
	 			if (lotteryNumbers.contains(x) && !guessedNumbers.contains(x))
	 			{
	 				correctNumbers++;
	 				guessedNumbers.add(x);
	 				System.out.println("Player has guess: " + x);
	 				
	 			}
 			}catch(NullPointerException e)
 			{
 				
 			}
 			
 			
		}
 		if(correctNumbers ==4)
 		{
 			fourNumber = true;
 			prizeCounter[3]++;
 			return "You won a four stars prize";
 		}
 		else if(correctNumbers >=5)
 		{
 			fiveNumbers = true;
 			fourNumber = false;
 			prizeCounter[4]++;

 			return "You won a five stars prize";
 		}
 		return "You didn't guess enough numbers";
		
	}
	
	public boolean isFiveNumberPrize()
	{
		return fiveNumbers;
	}
	
	public boolean isFourNumberPrize() 
	{
		return fourNumber;
	}
	
	public void reset() {
		fiveNumbers = false;
		fourNumber = false;
		
		lotteryNumbers = new ArrayList<>();
		

		lotteryNumbers = new ArrayList<Integer>();
		for(int i = 0; i < 5; i++)
		{
			lotteryNumbers.add(NumberGeneration.getRandomLotteryNumber());
			
		}
		for(int i = 0; i < lotteryNumbers.size(); i++)
		{
			System.out.print(lotteryNumbers.get(i) + "\t");
		}
		System.out.println("");
		
	}
	
	

	
	
	

}
