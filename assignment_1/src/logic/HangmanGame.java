package logic;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Random;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.text.Font;

public class HangmanGame  implements GameInterface<Character>{

	private int lives;
	private ArrayList<Character> secretWord;
	private ArrayList<String> wordList;
	private ArrayList<Label> secretWordLabels;
	private Random generator;
	private HBox secretWordPane;
	private int  letterCounter;
	private Integer[] starPrizes;
	
	public HangmanGame(HBox secretWordPane, Integer[] starPrizes)
	{
		this.starPrizes = starPrizes;
		this.secretWordPane = secretWordPane;
		this.letterCounter = 0;
		this.lives = 6;
		generator = new Random();
		wordList = FileManager.ReadHangManFile("Extra_Files/Words.txt");
		this.secretWordLabels = new ArrayList<Label>();
		selectWord();
	}
	
	
	@Override
	public void reset() {
		lives =6;
		
		letterCounter = 0;

		secretWordPane.getChildren().clear();

		
		selectWord();
		
		
	}
	
	private void selectWord() {
	
		secretWord = new ArrayList<Character>();
		secretWordLabels = new ArrayList<>();
		wordList.get(generator.nextInt(wordList.size())).chars().forEach((x) -> secretWord.add((char) x)) ;
		for(char x: secretWord){
			secretWordLabels.add(new Label("_"));
			secretWordLabels.get(secretWordLabels.size()-1).setFont(new Font(30));			
		}
		System.out.println(secretWord.stream().map((x)-> String.valueOf(x)).collect(Collectors.joining()));
		secretWordPane.getChildren().addAll(secretWordLabels);
	}

	@Override
	public String game(Character input) {
		
		
		if(lives > 0)
		{
			
			for(int i = 0; i < secretWord.size();i++ )
			{
				if (secretWord.get(i).equals(input))
				{
					secretWordLabels.get(i).setText(""+input);
					letterCounter++;
				}
				
			}
			
			if(letterCounter == secretWord.size()) 
			{
				starPrizes[2]++;
				return "CONGRATULATIONS, you won a three star prize";
				
			}	
			else if(secretWord.contains(input))
			{
				
				return "The word containts the letter " + input;
			}
			else
			{
				lives--;
				return "Wrong letter, you just have only "+ lives+ " lives";
			}
		}
		else
		{
			return "GAME OVER: The word was "+ secretWord.stream().map((x)-> x.toString()).collect(Collectors.joining());
		}
	}

	public void setLetter(String input, Label[] letters) 
	{
		int[] index = new int[]{0};
		secretWord.stream().forEach((x)->{ 
			if(x.equals(input)) {
				letters[index[0]].setText(input);
			}
		});
		
		index[0]++;
		
	}
	
	

}
