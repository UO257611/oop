package application;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Tab;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import logic.GameInterface;
import logic.NumberGuessGame;

public class GuessNumberTab extends Tab
{
	private final int BUTTON_INDEX_RESET = 1;
	private final int BUTTON_INDEX_QUIT = 0;
	private final int BUTTON_INDEX_CHECK = 2;
	private  TextField txtPlayerNumberGame;
	private Label lblGuessGameInfo;
	private GameInterface<Integer> guessControlGame;
	private Main mainWindow;
	private Integer[] starPrices;
	
	public GuessNumberTab(Main mainWindow, Integer[] starPrices ){
		this.mainWindow = mainWindow;
		this.starPrices = starPrices;
		this.setText("Guess Number");
		this.setContent(getGuessNumberTabContent());
	}
	
	
	public BorderPane getGuessNumberTabContent() {
		
		BorderPane generalPane = new BorderPane();
		VBox actualGame = new VBox();
		HBox controlGuessGameButtons = new HBox();
		Label lblWannaTry = new Label();
		guessControlGame = new NumberGuessGame(starPrices);
		Button btnCheckNumber = new Button("Check User Guess");
		Button btnQuit = new Button("Quit");
		Button btnReset = new Button("Reset");
		Button[] buttonList = new Button[] {btnQuit, btnReset, btnCheckNumber};
		generalPane.setCenter(actualGame);
		actualGame.setAlignment(Pos.CENTER);
		actualGame.getStyleClass().add("vBox");
		controlGuessGameButtons.setAlignment(Pos.CENTER);
		controlGuessGameButtons.setSpacing(5);
		
		
		lblWannaTry.setText("Wanna try?");
		lblWannaTry.setFont(new Font(40));
		lblGuessGameInfo = new Label();
		Label lblNumberGameLabel = new Label();
		lblNumberGameLabel.setText("This is the Guess Game.");
		lblGuessGameInfo.setFont(new Font(20));
		lblNumberGameLabel.setFont(new Font(40));
		txtPlayerNumberGame = new TextField();
		txtPlayerNumberGame.setMaxSize(500, 300);

		setUpGuessGameControlButtons(buttonList);
		actualGame.setSpacing(20);
		controlGuessGameButtons.getChildren().addAll(btnQuit,btnReset,btnCheckNumber);
		actualGame.getChildren().addAll(lblNumberGameLabel,lblWannaTry , txtPlayerNumberGame, lblGuessGameInfo);
		generalPane.setBottom(controlGuessGameButtons);
		return generalPane;
		
	}
	
	private void setUpGuessGameControlButtons(Button[] listOfButtons)
	{
		mainWindow.setUpGeneralButtons(listOfButtons);
		listOfButtons[BUTTON_INDEX_RESET].setOnAction(mainWindow.getResetHandler(guessControlGame).apply(lblGuessGameInfo));
		listOfButtons[BUTTON_INDEX_CHECK].setOnAction( new EventHandler<ActionEvent>() {
		
			@Override
			public void handle(ActionEvent event) {
				if(txtPlayerNumberGame.getText().equals(""))
					lblGuessGameInfo.setText("You have to write a number from 1-50");
				else {
					try {
						lblGuessGameInfo.setText(guessControlGame.game(Integer.parseInt(txtPlayerNumberGame.getText())));
						System.out.println(starPrices[1]);
						
						
					}catch(ClassCastException e ) {
						lblGuessGameInfo.setText("You have to introduce a number");
					}catch(NumberFormatException a) {
						lblGuessGameInfo.setText("You have to introduce a number");
					}
				}
			}
		});
		
		
		
	
	}
	

}
