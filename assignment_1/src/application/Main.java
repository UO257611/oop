package application;


import logic.Prize;
import logic.ThreadToUpdate;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.function.Function;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.event.EventType;
import javafx.geometry.Pos;
import javafx.stage.Modality;
import javafx.stage.Stage;
import logic.FileManager;
import logic.GameInterface;
import logic.HangmanGame;
import logic.LotteryGame;
import logic.NumberGuessGame;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.ImagePattern;
import javafx.scene.text.Font;

public class Main extends Application {

	private final int NUMBER_GUESS_GAME = 0;
	private final int NUMBER_LOTTERY_GAME = 1;
	private final int NUMBER_PRICES = 3;
	private final int BUTTON_INDEX_RESET = 1;
	private final int BUTTON_INDEX_QUIT = 0;
	private final int BUTTON_INDEX_CHECK = 2;
	
	private TabPane tabPane;
	private HashMap<Integer, ArrayList<Prize>> pricesMap;
	private Integer[] starPrizes;

	
	
	
	public EventHandler<ActionEvent> quitHandler = new EventHandler<ActionEvent>() {
		@Override
		public void handle(ActionEvent event) {
			Platform.exit();
			System.exit(0);
		}
	};
	

	@Override
	public void start(Stage primaryStage) {
		try {
			pricesMap  = FileManager.ReadPrizeFile("Extra_Files/Prizes");
			starPrizes = new Integer[5];
			for(int i = 0; i< starPrizes.length; i++)
				starPrizes[i] = 0;
			
			BorderPane root = new BorderPane();
				
			
			tabPane = new TabPane();
			Scene scene = new Scene(root,400,400);
			scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
			
			primaryStage.setScene(scene);
			primaryStage.show();
			
			
			PrizesTab prizesTab= new PrizesTab(this, starPrizes);
			GuessNumberTab guesNumberTab = new GuessNumberTab(this, starPrizes);
			LotteryTab lotteryTab = new LotteryTab(this, starPrizes);
			HangmanTab hangmanTab = new HangmanTab(this, starPrizes);
	        tabPane.getTabs().addAll(guesNumberTab,lotteryTab, hangmanTab,prizesTab);
	        tabPane.getTabs().get(NUMBER_PRICES).setDisable(true);
	        root.setCenter(tabPane);
			
	        
			ThreadToUpdate updateThread = new ThreadToUpdate(prizesTab.getLabelStarCounter(), starPrizes, prizesTab.getListPrizes(), pricesMap, prizesTab);
			updateThread.start();
			
			
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	
	public static void main(String[] args) {
		launch(args);
	}
	
	
	
	public void setUpGeneralButtons(Button[] listOfButtons) {
		for(Button theButton: listOfButtons)
		{
			theButton.setPrefSize(200,100);
		
		}
		
		listOfButtons[BUTTON_INDEX_QUIT].setOnAction(quitHandler);
	}
	
	public <T> Function<Label, EventHandler<ActionEvent>> getResetHandler(GameInterface<T> theGame)
	{
		return (Label x) -> {
			return new EventHandler<ActionEvent>() {
		
			@Override
			public void handle(ActionEvent event) {
				theGame.reset();
				x.setText("This is a new game");
			}
		};
		};
		
		
	}
	

	
	
	
}
