package application;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Tab;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import logic.GameInterface;
import logic.LotteryGame;

public class LotteryTab extends Tab
{

	private final int BUTTON_INDEX_RESET = 1;
	private final int BUTTON_INDEX_QUIT = 0;
	private final int BUTTON_INDEX_CHECK = 2;
	private TextField[] lotteryTextFields;
	private GameInterface<Integer[]> lotteryControlGame;
	private Label lblLotteryGameInfo;
	private Integer[] starPrices;
	private Main mainWindow;
	
	public LotteryTab(Main mainwindow, Integer[] starPrices) 
	{
		this.mainWindow = mainwindow;
		this.starPrices = starPrices;
		this.setText("Lotto");
		this.setContent(getLotteryTabContent());
		
	}
	
	public BorderPane getLotteryTabContent() {
		
		BorderPane generalLotteryPane = new BorderPane();
		VBox innerGame = new VBox();
		HBox controlLotteryButtons = new HBox();
		HBox lotteryInputPane = new HBox();
		Label lblWannaTry = new Label();
		lotteryControlGame= new LotteryGame(starPrices);
		Button btnCheckNumber = new Button("Check");
		Button btnQuit = new Button("Quit");
		Button btnReset = new Button("Reset");
		Button[] buttonList = new Button[] {btnQuit, btnReset, btnCheckNumber};
		
		lotteryTextFields = new TextField[5];
		
		for(int i = 0; i< 5; i++) {
			lotteryTextFields[i] = new TextField();
			lotteryInputPane.getChildren().add(lotteryTextFields[i]);
			lotteryTextFields[i].setPrefSize(75, 25);
		}
		
		lotteryInputPane.setSpacing(5);
		lotteryInputPane.setAlignment(Pos.CENTER);
		
		generalLotteryPane.setCenter(innerGame);
		innerGame.setAlignment(Pos.CENTER);
		controlLotteryButtons.setAlignment(Pos.CENTER);
		controlLotteryButtons.setSpacing(5);
		
		lblWannaTry.setText("Wanna try?");
		lblWannaTry.setFont(new Font(40));
		lblLotteryGameInfo = new Label();
		Label lblLotteryGameLabel = new Label();
		lblLotteryGameLabel.setText("This is the lottery Game.");
		lblLotteryGameInfo.setFont(new Font(20));
		lblLotteryGameLabel.setFont(new Font(40));
		
		setUpLotteryControlButtons(buttonList);
		innerGame.setSpacing(20);
		controlLotteryButtons.getChildren().addAll(btnQuit,btnReset,btnCheckNumber);
		innerGame.getChildren().addAll(lblLotteryGameLabel,lblWannaTry, lotteryInputPane, lblLotteryGameInfo);
		generalLotteryPane.setBottom(controlLotteryButtons);
		return generalLotteryPane;
	}
	
	
	private void setUpLotteryControlButtons(Button[] listOfButtons) {
		mainWindow.setUpGeneralButtons(listOfButtons);
		listOfButtons[BUTTON_INDEX_RESET].setOnAction(mainWindow.getResetHandler(lotteryControlGame).apply(lblLotteryGameInfo));
		listOfButtons[BUTTON_INDEX_CHECK].setOnAction(new EventHandler<ActionEvent>() {
			
			@Override
			public void handle(ActionEvent event) 
			{
				Integer[] lotteryNumbers = new Integer[5];
				boolean isANumber = true;
				for(int i = 0; i< lotteryNumbers.length; i++)
				{
					if(!lotteryTextFields[i].getText().isEmpty())
					{
						try {
							lotteryNumbers[i]=Integer.parseInt(lotteryTextFields[i].getText());
						}catch(NumberFormatException e) {
							isANumber =false;
							break;
						}
					}
				}
				if(!isANumber) {
					lblLotteryGameInfo.setText("You can only write numbers");
					
				}
				else {
					lblLotteryGameInfo.setText(lotteryControlGame.game(lotteryNumbers));
					
				}
					
					
				
			}
		});
		
	}

}
