package application;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.Tab;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.stage.Modality;
import javafx.stage.Stage;
import logic.Prize;

public class PrizesTab extends Tab
{
	
	private final int BUTTON_INDEX_RESET = 1;
	private final int BUTTON_INDEX_QUIT = 0;
	private final int BUTTON_INDEX_CHECK = 2;
	private Label lblStarXCounter;
	private Main mainWindow;
	private ListView<Prize> prizes;
	private Integer[] starPrizes;
	
	public PrizesTab (Main mainWindow, Integer[] prizesStar) {
		this.mainWindow = mainWindow;
		this.starPrizes = prizesStar;
		this.setText("Prize Selection");
		this.setContent(getPrizeTabContent());
	}
	
	public BorderPane getPrizeTabContent() {
		
		
		
		BorderPane generalPane = new BorderPane();
		VBox listPane = new VBox();
		VBox counterPane = new VBox();
		Label lblStarsCounterTitle = new Label("Prize counter");
		Label lblPrizeStatement = new Label("Choose a prize:");
		Button btnQuit = new Button("Quit");
		Button btnPrizeSelection = new Button("Select prize");
	

		lblStarsCounterTitle.setFont(new Font(40));	
		lblPrizeStatement.setFont(new Font(30));
		
		lblStarXCounter = new Label();
		counterPane.setAlignment(Pos.CENTER);
		listPane.setAlignment(Pos.CENTER);
		listPane.setSpacing(10);
		
		
		prizes =  new ListView<>();
		prizes.setMaxWidth(500);
		prizes.setMaxHeight(500);
		btnQuit.setOnAction(mainWindow.quitHandler);
		btnPrizeSelection.setOnAction(new EventHandler<ActionEvent>() {
			
			@Override
			public void handle(ActionEvent event) {
				Prize selectedPrize = prizes.getSelectionModel().getSelectedItem();
				if(selectedPrize != null) {
						
					starPrizes[selectedPrize.getWeight()-1]--;
					Stage dialog = new Stage();
					dialog.setMinHeight(350);
					dialog.setMinWidth(500);
					Button btnExitDialog = new Button("Close");
					btnExitDialog.setPrefSize(200, 50);
					btnExitDialog.setOnAction(new EventHandler<ActionEvent>() {
						@Override
						public void handle(ActionEvent event) {
								dialog.close();
						}
					});
					dialog .initModality(Modality.APPLICATION_MODAL);
					VBox dialogContent = new VBox();
					dialogContent.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
					dialogContent.setSpacing(20);
					dialogContent.setAlignment(Pos.CENTER);
					Label lblPrizeAnnouncement = new Label("CONGRATULATIONS!!! \nYou just won a " + selectedPrize.getRealPrize() );
					lblPrizeAnnouncement.setFont(new Font(30));
					dialogContent.getChildren().addAll(lblPrizeAnnouncement, btnExitDialog);
					Scene dialogScene = new Scene(dialogContent);
					dialog.setScene(dialogScene);
					dialog.show();
				}else {
					 
					
				}
				
			}
		});
		
		listPane.getChildren().addAll(lblPrizeStatement, prizes, btnPrizeSelection);
		counterPane.getChildren().addAll(lblStarsCounterTitle,lblStarXCounter);
		generalPane.setTop(counterPane);
		generalPane.setCenter(listPane);
		return generalPane;
	}

	public Label getLabelStarCounter()
	{
		return lblStarXCounter;
	}
	public ListView<Prize> getListPrizes(){
		return prizes;
	}
}
