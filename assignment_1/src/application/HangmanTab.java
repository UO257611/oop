package application;


import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.List;

import javax.swing.plaf.metal.MetalIconFactory.FileIcon16;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Tab;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import logic.HangmanGame;

public class HangmanTab  extends Tab
{


	private HBox wordHiddenPane;
	private HangmanGame gameController;
	private Main mainWindow;
	private Integer[] starPrizes;
	
	
	public HangmanTab ( Main mainWindow, Integer[] starPrizes) {
		
		this.starPrizes = starPrizes;
		this.setText("Hangman");
		this.mainWindow = mainWindow;
		wordHiddenPane = new HBox();
		gameController = new HangmanGame(wordHiddenPane, starPrizes);
		this.setContent(getHangManTabContent());
		
		
	}
	private BorderPane getHangManTabContent() {
		BorderPane generalPane = new BorderPane();
		VBox generalGamePane = new VBox();
		HBox buttonPane = new HBox();
		TextField txtUserLetter = new TextField();
		Label lblUserInfo = new Label("");
		Button btnCheck = new Button("Check letter");
		Button btnReset = new Button("Restart Game");
		
		
		wordHiddenPane.setSpacing(10);
		wordHiddenPane.setAlignment(Pos.CENTER);
		generalGamePane.setSpacing(10);
		generalGamePane.setAlignment(Pos.CENTER);
		buttonPane.setSpacing(10);
		buttonPane.setAlignment(Pos.CENTER);
		
		btnCheck.setPrefSize(200, 100);
		btnReset.setPrefSize(200, 100);
		txtUserLetter.setMaxSize(100, 25);
		lblUserInfo.setFont(new Font(30));

			
		
		btnCheck.setOnAction( new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				
				String input = txtUserLetter.getText().toUpperCase();
				if(input.length() == 1 && !input.matches("[0-9]"))
				{	
					lblUserInfo.setText(gameController.game(input.charAt(0)));
					
				}
				txtUserLetter.clear();
				
				
				
			}
		});
		
		btnReset.setOnAction(mainWindow.getResetHandler(gameController).apply(lblUserInfo));
		generalGamePane.getChildren().addAll(lblUserInfo ,wordHiddenPane, txtUserLetter);
		buttonPane.getChildren().addAll(btnReset, btnCheck);
		generalPane.setCenter(generalGamePane);
		generalPane.setBottom(buttonPane);
		
		return generalPane;
		
	}
	
}
